import { useState } from "react"
import { createContainer } from "unstated-next"

interface LocationParams {
  [key: string]: string | true
}

function pack(location: string, params: LocationParams): string {
  var hash = location
  Object.keys(params).forEach((k) => {
    const v = params[k]
    hash = v === true ? `${hash}&${k}` : `${hash}&${k}=${v}`
  })
  return hash.length === 0 ? "" : "#" + hash
}

function unpack(hash: string): [string, LocationParams] {
  const [location, ...rest] = hash.replace(/^#/, "").split("&")
  const params: LocationParams = {}
  rest.forEach((rawParam) => {
    const [k, v] = rawParam.split("=", 2)
    params[k] = v ? v : true
  })
  return [location, params]
}

export const LocationState = createContainer(() => {
  // At the start, initialize state based on the current window location hash.
  const [origLocation, origParams] = unpack(window.location.hash)
  const [location, setLocationState] = useState<string>(origLocation)
  const [params, setParamsState] = useState<LocationParams>(origParams)

  // When the browser window gets a new hash, update state to match.
  window.addEventListener("hashchange", () => {
    const [newLocation, newParams] = unpack(window.location.hash)
    setLocationState(newLocation)
    setParamsState(newParams)
  })

  // Function to set the (full) hash in the browser location state.
  const setHashFrom = (newLocation: string, newParams: LocationParams) => {
    const newFullHash = pack(newLocation, newParams)
    if (window.location.hash === newFullHash) return
    const hist = history // eslint-disable-line no-restricted-globals
    if (hist.pushState) {
      hist.pushState(null, "", newFullHash)
    } else {
      window.location.hash = newFullHash
    }
  }

  const setLocation = (newLocation: string) => {
    if (location === newLocation) return
    setHashFrom(newLocation, params)
    setLocationState(newLocation)
  }
  const setParam = (k: string, v: string | true) => {
    if (params.hasOwnProperty(k) && params[k] === v) return
    const newParams = { ...params, [k]: v }
    setHashFrom(location, newParams)
    setParamsState(newParams)
  }
  const clearParam = (k: string) => {
    if (!params.hasOwnProperty(k)) return
    const newParams = Object.assign({}, params)
    delete newParams[k]
    setHashFrom(location, newParams)
    setParamsState(newParams)
  }

  return { location, params, setLocation, setParam, clearParam }
})
