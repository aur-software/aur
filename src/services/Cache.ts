// import * as FileSystem from "expo-file-system"

// function cacheFilename(id: string) {
//   return FileSystem.cacheDirectory + id
// }

const CACHE: any = {}

// Write data to the cache for the given id.
// The given data is expected to be a JSON-serializable object.
// The data is written to the local file system on the device,
// in a directory that may sometimes be purged to free up space on the disk.
export async function cacheWrite(id: string, data: any) {
  // const filename = cacheFilename(id)

  // await FileSystem.writeAsStringAsync(filename, JSON.stringify(data))

  CACHE[id] = data
}

// Read data from the cache for the given id.
// If no data is found for the id, the promise will return with null.
// Otherwise it will return with the data as a JSON-deserialized object.
export async function cacheRead(id: string) {
  // const filename = cacheFilename(id)
  // const info = await FileSystem.getInfoAsync(filename)

  // if (info.exists) {
  //   return JSON.parse(await FileSystem.readAsStringAsync(filename))
  // } else {
  //   return null
  // }

  return id in CACHE ? CACHE[id] : null
}

// Try to read data by id from the cache.
// If not found in the cache, the given cache fill function is invoked,
// which is expected to call cacheWrite for this id and possibly others.
export async function cacheReadOrFillThenRead(
  id: string,
  fillFn: () => Promise<void>,
) {
  // If we already have this id in the cache, return the cached data.
  let result = await cacheRead(id)
  if (result) return result

  // Run the user-supplied cache fill function.
  await fillFn()

  // After the fill function has completed, we expect to have it in the cache.
  result = await cacheRead(id)
  return result!
}
