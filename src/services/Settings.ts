import { useState, useMemo } from "react"
import useAsyncEffect from "@n1ru4l/use-async-effect"
import { createContainer } from "unstated-next"
import { ScriptName, allScriptNames } from "./Scripts"
import { useDB } from "./LocalDB"

export type Settings = {
  notReady?: boolean
  displayScript: ScriptName
  darkTheme: boolean
  user: string
  remoteDBHost: string
}
function settingsFrom(doc: any) {
  return {
    // We explicitly don't pass through the "notReady" property.
    // Any settings that pass through this function will be considered ready.

    displayScript:
      allScriptNames.indexOf(doc.displayScript) >= 0
        ? doc.displayScript
        : "hebrew",

    darkTheme: "darkTheme" in doc ? !!doc.darkTheme : false,

    user: "user" in doc ? doc.user : "",

    remoteDBHost: "remoteDBHost" in doc ? doc.remoteDBHost : "",
  }
}
export const initialSettings: Settings = settingsFrom({})
initialSettings.notReady = true

export const SettingsState = createContainer(() => {
  const db = useMemo(() => useDB("settings"), [])

  const [settings, setSettings] = useState<Settings>(initialSettings)
  useAsyncEffect(function* (_, c) {
    const doc = yield* c(db.get("settings"))
    if (doc) setSettings(settingsFrom(doc))
    else setSettings(settingsFrom(settings))
  }, [])

  const setAndSaveSettings = (s: Settings) => {
    const newSettings = settingsFrom(s)
    setSettings(newSettings)
    db.put("settings", newSettings)
  }

  const setDisplayScript = (displayScript: ScriptName) =>
    setAndSaveSettings({ ...settings, displayScript })

  const setDarkTheme = (darkTheme: boolean) =>
    setAndSaveSettings({ ...settings, darkTheme })

  const setUser = (user: string) => setAndSaveSettings({ ...settings, user })

  const setRemoteDBHost = (remoteDBHost: string) =>
    setAndSaveSettings({ ...settings, remoteDBHost })

  return { settings, setDisplayScript, setDarkTheme, setUser, setRemoteDBHost }
})
