import { useState, useEffect } from "react"
import { createContainer } from "unstated-next"
import { SourceWord, SourceVerse } from "./SourceText"
import { digWord, DigResult, DigOp, summarizeOps } from "./Dig"
import { useDB } from "./LocalDB"
import initialSummaryDocs from "../data/init-db-analysis-summary.json"
import initialInterpretDocs from "../data/init-db-analysis-interpret.json"
import initialTranslateDocs from "../data/init-db-analysis-translate.json"
import useAsyncEffect from "@n1ru4l/use-async-effect"
import { SettingsState } from "./Settings"

// This is not actually a React hook, but because it says "use",
// this dumb eslint rule gets confused, so we disable it.
// eslint-disable-next-line react-hooks/rules-of-hooks
const db = useDB("analysis")
export const analysisDB = db

// Load initial analysis documents into the local database.
db.pouchdb.bulkDocs(initialSummaryDocs.docs, { new_edits: false })
db.pouchdb.bulkDocs(initialInterpretDocs.docs, { new_edits: false })
db.pouchdb.bulkDocs(initialTranslateDocs.docs, { new_edits: false })

// Return the entire contents of the local database, as a JSON string.
export async function exportDictionaryAsJSON(): Promise<string> {
  const { rows } = await db.pouchdb.allDocs({ include_docs: true })
  const docs = rows.map((info: any) => info.doc)
  return JSON.stringify({ new_edits: false, docs }, undefined, 2)
}

export type WordSummary = {
  word: string
  prefix: string
  main: string
  suffix: string
  notes: string
  dubious: boolean
  particle: boolean
  proper: boolean
}

export function makeWordSummary(word: string, input: any): WordSummary {
  return {
    word: word,
    prefix: input.prefix || "",
    main: input.main || (input.proper ? word : ""),
    suffix: input.suffix || "",
    notes: input.notes || "",
    dubious: Boolean(input.dubious),
    particle: Boolean(input.particle),
    proper: Boolean(input.proper),
  }
}

function saveableWordSummary(summary: WordSummary) {
  const doc: any = {}
  if (summary.prefix.length > 0) doc.prefix = summary.prefix
  if (summary.main.length > 0) doc.main = summary.main
  if (summary.suffix.length > 0) doc.suffix = summary.suffix
  if (summary.notes.length > 0) doc.notes = summary.notes
  if (summary.dubious) doc.dubious = true
  if (summary.particle) doc.particle = true
  if (summary.proper) {
    doc.main = ""
    doc.proper = true
  }
  return doc
}

export async function saveWordSummary(root: string, summary: WordSummary) {
  notifyRootSummarySubscribers(root, summary)
  return await db.put(`summary/aur/${root}`, saveableWordSummary(summary))
}

export async function deleteWordSummary(root: string) {
  notifyRootSummarySubscribers(root, undefined)
  return await db.put(`summary/aur/${root}`, {})
}

export async function lookupWordSummary(
  root: string,
  preventParticleOrProper: boolean,
) {
  const id = `summary/aur/${root}`
  const doc = await db.get(id)
  if (!doc) return undefined

  const summary = makeWordSummary(root, doc)

  if (preventParticleOrProper && (summary.proper || summary.particle))
    return undefined

  return summary
}

export type VerseTranslation = {
  user: string
  text: string
  date: Date
}

export function makeVerseTranslation(
  user: string,
  verseId: string,
  input: any,
): VerseTranslation {
  return {
    user: user,
    text: input.text || "",
    date: new Date(Date.parse(input.date || "0001-01-01T00:00:00Z")),
  }
}

function saveableVerseTranslation(translate: VerseTranslation) {
  const doc: any = {}
  if (translate.text.length > 0) doc.text = translate.text
  doc.date = translate.date.toISOString()
  return doc
}

export async function saveVerseTranslation(
  user: string,
  verseId: string,
  translate: VerseTranslation,
) {
  if (translate.text.length === 0)
    return await deleteVerseTranslation(user, verseId)

  return await db.put(
    `translate/${user}/${verseId}`,
    saveableVerseTranslation(translate),
  )
}

export async function deleteVerseTranslation(user: string, verseId: string) {
  return await db.put(`translate/${user}/${verseId}`, {})
}

export async function lookupVerseTranslation(user: string, verseId: string) {
  const id = `translate/${user}/${verseId}`
  const doc = await db.get(id)
  if (!doc) return undefined

  return makeVerseTranslation(user, verseId, doc)
}

export type WordInterpretation = {
  user: string
  root: string
  uids: number[]
  date: Date
}

export function makeWordInterpretation(
  user: string,
  wordId: string,
  input: any,
): WordInterpretation {
  return {
    user: user,
    root: input.root || "",
    uids: input.uids || [],
    date: new Date(Date.parse(input.date || "0001-01-01T00:00:00Z")),
  }
}

function saveableWordInterpretation(interpret: WordInterpretation) {
  const doc: any = {}
  if (interpret.root.length > 0) doc.root = interpret.root
  if (interpret.uids.length > 0) doc.uids = interpret.uids
  doc.date = interpret.date.toISOString()
  return doc
}

export async function saveWordInterpretation(
  user: string,
  wordId: string,
  interpret: WordInterpretation,
) {
  return await db.put(
    `interpret/${user}/${wordId}`,
    saveableWordInterpretation(interpret),
  )
}

export async function deleteWordInterpretation(user: string, wordId: string) {
  return await db.put(`interpret/${user}/${wordId}`, {})
}

export async function lookupWordInterpretation(user: string, wordId: string) {
  const id = `interpret/${user}/${wordId}`
  const doc = await db.get(id)
  if (!doc) return undefined

  return makeWordInterpretation(user, wordId, doc)
}

function fillAnalysisFromWordInterpretation(
  analysis: Analysis | undefined,
  interpret: WordInterpretation | undefined,
): Analysis | undefined {
  if (!analysis || !interpret) return analysis

  const savedResultIndex = analysis.results.findIndex(
    (result) =>
      result.dig.root === interpret.root &&
      result.dig.ops.length === interpret.uids.length &&
      result.dig.ops.every((op, index) => op.uid === interpret.uids[index]),
  )

  return { ...analysis, savedResultIndex }
}

export function applyDigToRootSummary(
  rootSummary: WordSummary,
  ops: DigOp[],
): WordSummary {
  const [preParts, postParts] = summarizeOps(ops)
  let prefix = rootSummary.prefix
  let suffix = rootSummary.suffix

  preParts.forEach((part) => {
    prefix = prefix ? part + " " + prefix : part
  })
  postParts.forEach((part) => {
    suffix = suffix ? suffix + " " + part : part
  })

  return { ...rootSummary, prefix, suffix }
}

export type AnalysisAlternative = {
  rootSummary: WordSummary | undefined
  dig: DigResult
}

export type Analysis = {
  savedResultIndex?: number
  resultIndex: number
  results: AnalysisAlternative[]
  knownResultCount: number
  knownRoots: Set<string>
  allRoots: Set<string>
}

// Given a list of analysis alternative results, return a full analysis.
function analysisFromResults(results: AnalysisAlternative[]): Analysis {
  // We choose the first result that found a valid root summary.
  const resultIndex = results.findIndex((result) => result.rootSummary)

  let knownResultCount = 0
  const knownRoots = new Set<string>()
  const allRoots = new Set<string>()
  results.forEach((result) => {
    if (result.rootSummary) {
      knownResultCount += 1
      knownRoots.add(result.dig.root)
    }
    allRoots.add(result.dig.root)
  })

  return {
    resultIndex,
    results,
    knownResultCount,
    knownRoots,
    allRoots,
  }
}

// Get the full analysis for the given word, asynchronously.
async function getAnalysis(word: string): Promise<Analysis> {
  return analysisFromResults(
    await Promise.all(
      digWord(word).map(async (dig) => {
        const rootSummary =
          dig.root.length === 0
            ? makeWordSummary("", {})
            : await lookupWordSummary(
                dig.root,
                dig.ops.some((op) => op.preventParticleOrProper),
              )
        return { rootSummary, dig }
      }),
    ),
  )
}

// Keep track of callbacks that want to be subscribed about changes to a root.
type RootSummaryCallback = (
  root: string,
  newSummary: WordSummary | undefined,
) => boolean
const rootSummarySubscribers: { [root: string]: Set<RootSummaryCallback> } = {}

// Notify all subscribers for the given root of a new summary being available.
// Remove any subscribers that request to be cancelled from the mapping.
function notifyRootSummarySubscribers(
  root: string,
  newSummary: WordSummary | undefined,
) {
  // Get the callbacks currently subscribed for this root.
  const callbacks = rootSummarySubscribers[root]
  if (!callbacks) return

  // Call each callback. If it returns true, we treat it as a cancel, and we
  // gather all cancelled callbacks into a list. We go to this extra effort
  // so that we can avoid mutating the Set that we're currently iterating over.
  const cancelledCallbacks: RootSummaryCallback[] = []
  callbacks.forEach((callback) => {
    const didCancel = callback(root, newSummary)
    if (didCancel) cancelledCallbacks.push(callback)
  })

  // Remove each cancelled callback from the set.
  // Then remove the set if it has become empty.
  cancelledCallbacks.forEach((callback) => callbacks.delete(callback))
  if (callbacks.size === 0) delete rootSummarySubscribers[root]
}

// Subscribe with the given callback for any analysis changes to the given word.
// The callback will be called once immediately after an initial analysis,
// then it will be called again for every change to the analysis over time.
// If it ever returns true, the subscription will be cancelled.
function subscribeAnalysis(word: string, callback: (a: Analysis) => boolean) {
  ;(async () => {
    // Fetch an initial analysis and forward it to the callback.
    // If the callback returns a request to cancel, we will go no further.
    let analysis = await getAnalysis(word)
    const didCancel = callback(analysis)
    if (didCancel) return

    // The underlying subscription engine issues a callback whenever an
    // individual root summary has been created/updated/deleted.
    // We need to wrap that in a function that will create a modified full
    // analysis, based on the initial analysis, any time that any root
    // that was in the result set of the initial analysis has changed.
    const wrappedCallback: RootSummaryCallback = (root, newSummary) => {
      // Update the running analysis kept in our closure from above.
      analysis = analysisFromResults(
        analysis.results.map((result) =>
          result.dig.root !== root
            ? result
            : { ...result, rootSummary: newSummary },
        ),
      )

      // Call the callback and forward the boolean (cancel) return value of it.
      return callback(analysis)
    }

    // For each root in the analysis results, subscribe our wrapped callback.
    analysis.allRoots.forEach((root) => {
      let callbacks = rootSummarySubscribers[root]
      if (!callbacks) {
        callbacks = new Set<RootSummaryCallback>()
        rootSummarySubscribers[root] = callbacks
      }
      callbacks.add(wrappedCallback)
    })
  })().catch((e) => console.error(e))
}

export const WordAnalysisState = createContainer(
  (word: SourceWord | undefined) => {
    if (!word) throw new Error("initialState={word} is a required prop")

    // The user is grabbed directly from the settings
    const { settings } = SettingsState.useContainer()
    const user = settings.user

    // Word interpretation lookup is done by user and word id.
    const [interpret, setInterpret] = useState<WordInterpretation | undefined>(
      undefined,
    )
    useAsyncEffect(
      function* (_, c) {
        setInterpret(yield* c(lookupWordInterpretation(user, word.id)))
      },
      [user, word.id],
    )

    // Analysis is continuously updated via subscription,
    // so that when one word's root summary is changed, it is also reflected
    // across all other instances of the same root that are currently in the app.
    const [analysis, setAnalysis] = useState<Analysis | undefined>(undefined)
    useEffect(() => {
      let didCancel = false

      subscribeAnalysis(word.word, (a) => {
        if (!didCancel) setAnalysis(a)
        return didCancel
      })

      return () => {
        didCancel = true
      }
    }, [word.word])

    const saveWordInterpretationIndex = (newIndex: number) => {
      const dig = analysis?.results[newIndex]?.dig
      if (!dig) return

      const newInterpret: WordInterpretation = {
        user,
        root: dig.root,
        uids: dig.ops.map((op) => op.uid),
        date: new Date(),
      }
      saveWordInterpretation(user, word.id, newInterpret).then(() => {
        setInterpret(newInterpret)
      })
    }

    const clearWordInterpretation = () => {
      deleteWordInterpretation(user, word.id).then(() => {
        setInterpret(undefined)
      })
    }

    return {
      analysis: fillAnalysisFromWordInterpretation(analysis, interpret),
      saveWordInterpretationIndex,
      clearWordInterpretation,
    }
  },
)

export const VerseAnalysisState = createContainer(
  (verse: SourceVerse | undefined) => {
    if (!verse) throw new Error("initialState={verse} is a required prop")

    // The user is grabbed directly from the settings
    const { settings } = SettingsState.useContainer()
    const user = settings.user

    // Word interpretation lookup is done by user and word id.
    const [translation, setTranslation] = useState<
      VerseTranslation | undefined
    >(undefined)
    useAsyncEffect(
      function* (_, c) {
        setTranslation(yield* c(lookupVerseTranslation(user, verse.id)))
      },
      [user, verse.id],
    )

    return {
      translation,
      saveVerseTranslation: (text: string) =>
        saveVerseTranslation(user, verse.id, { user, text, date: new Date() }),
    }
  },
)
