import PouchDB from "pouchdb"

export function useDB(nameOfDB: string) {
  const db = new PouchDB(nameOfDB)

  const get = async (id: string) => {
    try {
      return await db.get(id)
    } catch (err) {
      if (err.name !== "not_found") throw err
      return undefined
    }
  }

  const getCurrentRev = async (id: string) => {
    return (await get(id))?._rev
  }

  const put = async (id: string, doc: any) => {
    const rev = await getCurrentRev(id)
    if (Object.entries(doc).length === 0) {
      return await db.put(
        { _id: id, _rev: rev, _deleted: true },
        { force: true },
      )
    } else {
      return await db.put({ _id: id, _rev: rev, ...doc }, { force: true })
    }
  }

  const pouchdb = db
  return { pouchdb, get, getCurrentRev, put }
}
