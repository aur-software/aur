import fetch from "node-fetch"
// import retryBase from "async-await-retry"
import { DOMParser } from "xmldom"
import * as xmldom from "xmldom-js"
import { latexParser } from "latex-parser"
import { cacheWrite, cacheReadOrFillThenRead } from "./Cache"
import useAsyncEffect from "@n1ru4l/use-async-effect"
import { useState } from "react"

// // Define a retry with specific options, based on the async-await-retry library.
// async function retry(fn: any) {
//   return await retryBase(fn, null, {
//     retriesMax: 10,
//     interval: 50,
//   })
// }
async function retry(fn: any) {
  return await fn()
}

export type SourceBook = {
  id: string
  name: string
  source: string
  chapters: SourceChapter[]
}

export type SourceChapter = {
  id: string
  name: string
  source: string
  verses: SourceVerse[]
}

export type SourceVerse = {
  id: string
  name: string
  source: string
  words: SourceWord[]
}

export type SourceWord = {
  id: string
  source: string
  word: string
}

const TRANSLITERATE: { [key: string]: string } = {
  א: "A",
  ב: "B",
  ג: "G",
  ד: "D",
  ה: "E",
  ו: "U",
  ז: "Z",
  ח: "H",
  ט: "Th",
  י: "Y",
  כ: "K",
  ך: "K",
  ל: "L",
  מ: "M",
  ם: "M",
  נ: "N",
  ן: "N",
  ס: "S",
  ע: "O",
  פ: "P",
  ף: "P",
  צ: "Ts",
  ץ: "Ts",
  ק: "Q",
  ר: "R",
  ש: "Sh",
  ת: "T",
}

const UNTRANSLITERATE: { [key: string]: string[] } = Object.entries(
  TRANSLITERATE,
).reduce((ret: { [key: string]: string[] }, [key, value]) => {
  const list = ret[value] || []
  list.push(key)
  ret[value] = list
  return ret
}, {})

// Define the list of books in the WLC, by bookId, in the WLC order.
const wlcBooks = new Map([
  [
    "gen",
    { name: "Genesis", mcId: "Gen", chapterCount: 50, source: "בְּרֵאשִׁית" },
  ],
  [
    "exod",
    { name: "Exodus", mcId: "Exo", chapterCount: 40, source: "שְׁמֹות" },
  ],
  [
    "lev",
    { name: "Leviticus", mcId: "Lev", chapterCount: 27, source: "וַיִּקְרָא" },
  ],
  [
    "num",
    { name: "Numbers", mcId: "Num", chapterCount: 36, source: "בְּמִדְבַּר" },
  ],
  [
    "deut",
    { name: "Deuteronomy", mcId: "Deu", chapterCount: 34, source: "דְּבָרִים" },
  ],
  [
    "josh",
    { name: "Joshua", mcId: "Jos", chapterCount: 24, source: "יְהוֹשֻעַ" },
  ],
  [
    "judg",
    { name: "Judges", mcId: "Jdg", chapterCount: 21, source: "שֹׁפְטִים" },
  ],
  [
    "1sam",
    {
      name: "1 Samuel",
      mcId: "Sam",
      mcId2: "1Sa",
      chapterCount: 31,
      source: "1 שְׁמוּאֵל",
    },
  ],
  [
    "2sam",
    {
      name: "2 Samuel",
      mcId: "Sam",
      mcId2: "2Sa",
      chapterCount: 24,
      source: "2 שְׁמוּאֵל",
    },
  ],
  [
    "1kgs",
    {
      name: "1 Kings",
      mcId: "Kin",
      mcId2: "1Ki",
      chapterCount: 22,
      source: "1 מְלָכִים",
    },
  ],
  [
    "2kgs",
    {
      name: "2 Kings",
      mcId: "Kin",
      mcId2: "2Ki",
      chapterCount: 25,
      source: "2 מְלָכִים",
    },
  ],
  [
    "isa",
    { name: "Isaiah", mcId: "Isa", chapterCount: 66, source: "יְשַׁעְיָהוּ" },
  ],
  [
    "jer",
    { name: "Jeremiah", mcId: "Jer", chapterCount: 52, source: "יִרְמְיָהוּ" },
  ],
  [
    "ezek",
    { name: "Ezekiel", mcId: "Eze", chapterCount: 48, source: "יְחֶזְקֵאל" },
  ],
  [
    "hos",
    {
      name: "Hosea",
      mcId: "Min",
      mcId2: "Hos",
      chapterCount: 14,
      source: "הוֹשֵׁעַ",
    },
  ],
  [
    "joel",
    {
      name: "Joel",
      mcId: "Min",
      mcId2: "Joe",
      chapterCount: 3,
      source: "יוֹאֵל",
    },
  ],
  [
    "amos",
    {
      name: "Amos",
      mcId: "Min",
      mcId2: "Amo",
      chapterCount: 9,
      source: "עָמוֹס",
    },
  ],
  [
    "obad",
    {
      name: "Obadiah",
      mcId: "Min",
      mcId2: "Oba",
      chapterCount: 1,
      source: "עֹבַדְיָה",
    },
  ],
  [
    "jonah",
    {
      name: "Jonah",
      mcId: "Min",
      mcId2: "Jon",
      chapterCount: 4,
      source: "יוֹנָה",
    },
  ],
  [
    "mic",
    {
      name: "Micah",
      mcId: "Min",
      mcId2: "Mic",
      chapterCount: 7,
      source: "מִיכָה",
    },
  ],
  [
    "nah",
    {
      name: "Nahum",
      mcId: "Min",
      mcId2: "Nah",
      chapterCount: 3,
      source: "נַחוּם",
    },
  ],
  [
    "hab",
    {
      name: "Habakkuk",
      mcId: "Min",
      mcId2: "Hab",
      chapterCount: 3,
      source: "חֲבַקּוּק",
    },
  ],
  [
    "zeph",
    {
      name: "Zephaniah",
      mcId: "Min",
      mcId2: "Zep",
      chapterCount: 3,
      source: "צְפַנְיָה",
    },
  ],
  [
    "hag",
    {
      name: "Haggai",
      mcId: "Min",
      mcId2: "Hag",
      chapterCount: 2,
      source: "חַגַּי",
    },
  ],
  [
    "zech",
    {
      name: "Zechariah",
      mcId: "Min",
      mcId2: "Zec",
      chapterCount: 14,
      source: "זְכַרְיָה",
    },
  ],
  [
    "mal",
    {
      name: "Malachi",
      mcId: "Min",
      mcId2: "Mal",
      chapterCount: 4,
      source: "מַלְאָכִי",
    },
  ],
  [
    "1chr",
    {
      name: "1 Chronicles",
      mcId: "Chr",
      mcId2: "1Ch",
      chapterCount: 29,
      source: "1 דִּבְרֵי הַיָּמִים",
    },
  ],
  [
    "2chr",
    {
      name: "2 Chronicles",
      mcId: "Chr",
      mcId2: "2Ch",
      chapterCount: 36,
      source: "2 דִּבְרֵי הַיָּמִים",
    },
  ],
  [
    "ps",
    { name: "Psalms", mcId: "Psa", chapterCount: 150, source: "תְהִלִּים" },
  ],
  ["job", { name: "Job", mcId: "Job", chapterCount: 42, source: "אִיּוֹב" }],
  [
    "prov",
    { name: "Proverbs", mcId: "Pro", chapterCount: 31, source: "מִשְׁלֵי" },
  ],
  ["ruth", { name: "Ruth", mcId: "Rut", chapterCount: 4, source: "רוּת" }],
  [
    "song",
    {
      name: "Song of Songs",
      mcId: "Sol",
      chapterCount: 8,
      source: "שִׁיר הַשִּׁירִים",
    },
  ],
  [
    "eccl",
    { name: "Ecclesiastes", mcId: "Ecc", chapterCount: 12, source: "קֹהֶלֶת" },
  ],
  [
    "lam",
    { name: "Lamentations", mcId: "Lam", chapterCount: 5, source: "אֵיכָה" },
  ],
  [
    "esth",
    { name: "Esther", mcId: "Est", chapterCount: 10, source: "אֶסְתֵר" },
  ],
  [
    "dan",
    { name: "Daniel", mcId: "Dan", chapterCount: 12, source: "דָּנִיֵּאל" },
  ],
  [
    "ezra",
    {
      name: "Ezra",
      mcId: "Ezr",
      mcId2: "Ezr",
      chapterCount: 10,
      source: "עֶזְרָא",
    },
  ],
  [
    "neh",
    {
      name: "Nehemiah",
      mcId: "Ezr",
      mcId2: "Neh",
      chapterCount: 13,
      source: "נְחֶמְיָ֖ה",
    },
  ],
])

export function allHebrewGlyphs(): string[] {
  var seenTrans: string[] = []
  return Object.keys(TRANSLITERATE).filter((glyph) => {
    const trans = TRANSLITERATE[glyph]!
    if (seenTrans.includes(trans)) return false
    seenTrans.push(trans)
    return true
  })
}

export function transliterateHebrewWord(source: string): string {
  return source.slice().replace(/./g, (grapheme) => {
    if (grapheme === " ") return " "
    return TRANSLITERATE[grapheme] || ""
  })
}

export function untransliterateToHebrewWord(word: string): string {
  return word
    .slice()
    .replace(/[A-Z][a-z]?$/, (sound) => {
      return (UNTRANSLITERATE[sound] || [""]).slice(-1)[0]
    })
    .replace(/[A-Z][a-z]?/g, (sound) => {
      return (UNTRANSLITERATE[sound] || [""])[0]
    })
}

export function buildChapterId(bookId: string, index: number): string {
  return bookId + "." + index.toString().padStart(3, "0")
}

export function buildVerseId(chapterId: string, index: number): string {
  return chapterId + "." + index.toString().padStart(3, "0")
}

export function buildWordId(verseId: string, index: number): string {
  return verseId + "." + index.toString().padStart(3, "0")
}

export function decomposeChapterId(chapterId: string): [string, number] {
  const [bookId, chapterNum] = chapterId.split(".", 2)
  return [bookId, parseInt(chapterNum)]
}

export function decomposeVerseId(verseId: string): [string, number] {
  const [bookId, chapterNum, verseNum] = verseId.split(".", 3)
  return [buildChapterId(bookId, parseInt(chapterNum)), parseInt(verseNum)]
}

export function splitWordId(wordId: string): [string, number, number, number] {
  const [bookId, chapterNum, verseNum, wordNum] = wordId.split(".", 4)
  return [bookId, parseInt(chapterNum), parseInt(verseNum), parseInt(wordNum)]
}

export function bookName(bookId: string): string {
  const bookInfo = wlcBooks.get(bookId)
  return bookInfo ? bookInfo.name : ""
}

export function bookSource(bookId: string): string {
  const bookInfo = wlcBooks.get(bookId)
  return bookInfo ? bookInfo.source : ""
}

export function buildVerseName(verseId: string): string {
  const [book, chapter, verse] = verseId.split(".", 3)

  const displayBook = bookName(book)
  const displayChapter = chapter.replace(/^0+/, "")
  const displayVerse = verse.replace(/^0+/, "")

  return `${displayBook} ${displayChapter}:${displayVerse}`
}

const xmlToJSchema = {
  osis: {
    osisText: {
      div: {
        chapter: [
          {
            verse: [
              {
                w: [
                  {
                    "": xmldom.content(xmldom.string),
                    id: xmldom.attribute(),
                    // // TODO: use the following attributes
                    // lemma: xmldom.attribute(),
                    // morph: xmldom.attribute(),
                  },
                ],
              },
            ],
          },
        ],
      },
    },
  },
}

type Input = { osis: { osisText: { div: { chapter: InputChapter[] } } } }
type InputChapter = { verse: InputVerse[] }
type InputVerse = { w: InputWord[] }
type InputWord = {
  "": string
  id: string
}

export async function downloadAndParseBook(
  editionId: string,
  bookId: string,
): Promise<SourceBook> {
  switch (editionId) {
    case "wlc":
      return downloadAndParseBookWLC(bookId)
    case "mc":
      return downloadAndParseBookMC(bookId)
    default:
      throw new Error(`no download source for edition: ${editionId}`)
  }
}

// Define a function that downloads a given book from the WLC XML and parses it.
async function downloadAndParseBookWLC(bookId: string): Promise<SourceBook> {
  const bookInfo = wlcBooks.get(bookId)!
  const bookXmlKey = bookId.replace(/[a-z](?=[a-z]+)/, (c) => c.toUpperCase())

  // Fetch from our mirror of the openscriptures/morphhb repo on GitHub.
  const resp = await retry(async () =>
    fetch(
      "https://gitlab.com/aur-software/morphhb-mirror/-/raw/" +
        `2bdcd48c770370f4ee24ad89145a9944677ece3f/wlc/${bookXmlKey}.xml`,
    ),
  )

  // Get the response body as XML.
  const xmlText = await resp.text()
  const xml = new DOMParser().parseFromString(xmlText, "text/xml")

  const xmlData = xmldom.readXML(xml, xmlToJSchema, xmldom.ignoreNamespace)
  return {
    id: bookId,
    name: `${bookInfo.name}`,
    source: `${bookInfo.source}`,
    chapters: (xmlData as Input).osis.osisText.div.chapter.map(
      (chapter: InputChapter, chapterIndex: number) => {
        const chapterId = buildChapterId(bookId, chapterIndex + 1)
        return {
          id: chapterId,
          name: `${bookInfo.name} ${chapterIndex + 1}`,
          source: `${bookInfo.source} ${chapterIndex + 1}`,
          verses: chapter.verse.map((verse: InputVerse, verseIndex: number) => {
            const verseId = buildVerseId(chapterId, verseIndex + 1)
            return {
              id: verseId,
              name: `${bookInfo.name} ${chapterIndex + 1}:${verseIndex + 1}`,
              source: `${bookInfo.source} ${chapterIndex + 1}:${
                verseIndex + 1
              }`,
              words: verse.w.map((w: InputWord, wIndex: number) => {
                return {
                  id: buildWordId(verseId, wIndex + 1), // TODO: use w.id instead of wNum
                  ref: w.id,
                  source: w[""],
                  word: transliterateHebrewWord(w[""]),
                }
              }),
            }
          }),
        }
      },
    ),
  }
}

// TODO: account for final-shaped letters
// "k\n": "ך\n",
// "m\n": "ם\n",
// "p\n": "ף\n",
// "Y\n": "ץ\n",
// "n\n": "ן\n",
const mcTransformPattern = /(?<ignore><.*?>)|(H[EaA]|W\/|.|\n)/g
const mcTransform: { [key: string]: string } = {
  "'": "א",
  b: "ב",
  d: "ד",
  g: "ג",
  h: "ה",
  w: "ו",
  z: "ז",
  x: "ח",
  T: "ט",
  y: "י",
  k: "כ",
  l: "ל",
  m: "מ",
  n: "נ",
  s: "ס",
  "`": "ע",
  p: "פ",
  Y: "צ",
  q: "ק",
  r: "ר",
  S: "שׁ", // "shin"
  W: "שׂ", // "sin"
  "W/": "ש", // neither "shin" nor "sin"?
  t: "ת",
  '"': "\u05B0", // "sheva"
  HE: "\u05B1", // "hataf segol"
  Ha: "\u05B2", // "hataf patah"
  HA: "\u05B3", // "hataf qamats"
  i: "\u05B4", // "hiriq"
  e: "\u05B5", // "tsere"
  E: "\u05B6", // "segol"
  a: "\u05B7", // "patah"
  A: "\u05B8", // "qamats"
  o: "\u05B9", // "holam"
  "^": "\u05BA", // "holam haser for vav"
  u: "\u05BB", // "qubuts"
  "*": "\u05BC", // "dagesh"
  "=": "\n", // "maqaf" becomes newline, because it is a word separator
  "\n": "\n", // newline pass-through - this is the main word separator
  ":": "", // verse end designator is ignored - we already know when verses end
}

// Define a function that downloads a given book from the MC XML and parses it.
async function downloadAndParseBookMC(bookId: string): Promise<SourceBook> {
  const bookInfo = wlcBooks.get(bookId)!

  // Fetch from our mirror of the openscriptures/morphhb repo on GitHub.
  const resp = await retry(async () =>
    fetch(
      "https://gitlab.com/aur-software/aivazian-tnk/-/raw/" +
        `5b6200962fba6fe775a1a6780bab038f79ffdfe5/inp/${bookInfo.mcId}.inp`,
    ),
  )

  // Get the response body as Latex Tokens.
  const latexText = await resp.text()
  const tokens = latexParser
    .parse(latexText)
    .value?.filter(
      (token) =>
        "type" in token &&
        (token.type === "TeXRaw" ||
          (token.type === "TeXComm" && token.name === "vs")),
    )

  // Create the book object we will return after filling it.
  // In doing so we also create a temporary empty chapter and verse,
  // only for the purposes of making the typing of these variables automatic.
  let latestVerse = {
    id: "",
    name: "",
    source: "",
    words: [{ id: "", ref: "", source: "", word: "" }],
  }
  let latestChapter = {
    id: "",
    name: "",
    source: "",
    verses: [latestVerse],
  }
  const book = {
    id: bookId,
    name: `${bookInfo.name}`,
    source: `${bookInfo.source}`,
    chapters: [latestChapter],
  }
  book.chapters.pop() // pop the fake empty chapter now that types are settled

  tokens?.forEach((token) => {
    if ("type" in token && token?.type === "TeXRaw") {
      // We got some source text. Parse it out and convert to Masoretic Hebrew,
      // ignoring other notation, and splitting by word.
      const sourceWords = [...token.text.matchAll(mcTransformPattern)]
        .filter((match) => !match.groups?.ignore)
        .map((match) => mcTransform[match[0]] ?? `(${match[0]})`)
        .join("")
        .split("\n")
        .filter((sourceWord) =>
          sourceWord.match(/[אבגדהוזחטיכךלמםנןסעפףצץרקשת]/),
        )

      // Now add the parsed words to the latest verse.
      sourceWords.forEach((source) => {
        latestVerse.words.push({
          id: buildWordId(latestVerse.id, latestVerse.words.length + 1),
          ref: "", // TODO: correlate to morphhb refs in WLC
          source: source,
          word: transliterateHebrewWord(source),
        })
      })
    } else if ("arguments" in token) {
      // We got a new verse header. Parse out the chapter and verse number.
      const verseHeader = token.arguments?.[0].latex.map((t) =>
        "text" in t ? t.text : undefined,
      )[0]
      const [mcId2, chapterVerseNums] = verseHeader?.split(" ")
      const [chapterNum, verseNum] = chapterVerseNums
        ?.split(":")
        .map((numText) => parseInt(numText))

      // Only proceed if this is a verse for the book we wanted in this file.
      if (mcId2 === (bookInfo.mcId2 ?? bookInfo.mcId)) {
        // Create chapter in book if it does not already exist.
        const chapterId = buildChapterId(bookId, chapterNum)
        if (latestChapter.id !== chapterId) {
          latestChapter = {
            id: chapterId,
            name: `${bookInfo.name} ${chapterNum}`,
            source: `${bookInfo.source} ${chapterNum}`,
            verses: [],
          }
          book.chapters.push(latestChapter)
        }

        // Create verse in chapter if it does not already exist.
        const verseId = buildVerseId(chapterId, verseNum)
        if (latestVerse.id !== verseId) {
          latestVerse = {
            id: verseId,
            name: `${bookInfo.name} ${chapterNum}:${verseNum}`,
            source: `${bookInfo.source} ${chapterNum}:${verseNum}`,
            words: [],
          }
          latestChapter.verses.push(latestVerse)
        }
      }
    }
  })

  return book
}

async function fetchBook(editionId: string, bookId: string) {
  return (await (
    await fetch(`${editionId}/books/${bookId}.json`)
  ).json()) as SourceBook
}

export async function fetchBookFromFs(
  fs: any,
  editionId: string,
  bookId: string,
) {
  return JSON.parse(
    await fs.promises.readFile(pathForBookOnFs(editionId, bookId)),
  ) as SourceBook
}

export function pathForBookOnFs(editionId: string, bookId: string): string {
  return `public/${editionId}/books/${bookId}.json`
}

export function listEditions() {
  return [{ id: "wlc" }, { id: "mc" }]
}

export function listBooks() {
  const list: { name: string; bookId: string }[] = []
  wlcBooks.forEach(({ name }, bookId) => {
    list.push({ name, bookId })
  })
  return list
}

export function listChapters(bookId: string) {
  const list: { number: number; chapterId: string }[] = []

  const chapterCount = wlcBooks.get(bookId)?.chapterCount
  if (!chapterCount) return list

  return Array.from({ length: chapterCount }, (_, i) => {
    const number = i + 1
    return { number, chapterId: buildChapterId(bookId, number) }
  })
}

export async function getBook(
  editionId: string,
  bookId: string,
): Promise<SourceBook> {
  const fullBookId = `${editionId}.${bookId}`
  return cacheReadOrFillThenRead(fullBookId, async () => {
    cacheWrite(fullBookId, await fetchBook(editionId, bookId))
  })
}

export async function getChapter(
  editionId: string,
  chapterId: string,
): Promise<SourceChapter> {
  const [bookId, chapterNum] = decomposeChapterId(chapterId)
  return (await getBook(editionId, bookId)).chapters[chapterNum - 1]
}

export function useBook(
  editionId: string,
  bookId: string,
): SourceBook | undefined {
  const [result, setResult] = useState<SourceBook | undefined>()
  useAsyncEffect(
    function* (_, c) {
      setResult(yield* c(getBook(editionId, bookId)))
    },
    [editionId, bookId],
  )
  return result
}

export function useChapter(
  editionId: string,
  chapterId: string,
): SourceChapter | undefined {
  const [result, setResult] = useState<SourceChapter | undefined>()
  useAsyncEffect(
    function* (_, c) {
      setResult(yield* c(getChapter(editionId, chapterId)))
    },
    [editionId, chapterId],
  )
  return result
}
