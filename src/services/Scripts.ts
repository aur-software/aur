import { Settings } from "./Settings"
import { transliterateHebrewWord } from "./SourceText"

// These are the valid scripts we support, each one supporting the glyphs
// that we want to display for each word on the screen.
export type ScriptName =
  | "hebrew"
  | "masoretic"
  | "protosemitic"
  | "phoenician"
  | "obry"
  | "tml"

export const allScriptNamesAndLabels = [
  { name: "hebrew", label: "Hebrew" },
  { name: "masoretic", label: "Masoretic" },
  { name: "protosemitic", label: "Proto-Semitic" },
  { name: "phoenician", label: "Phoenician" },
  { name: "obry", label: "Obry" },
  { name: "tml", label: "Transliterated (matres lectionis)" },
]
export const allScriptNames = allScriptNamesAndLabels.map((o) => o.name)

// Each custom font we reference uses a particular set of ascii characters
// for their display of the associated characters of that script.
// Here we define translation tables from hebrew letters to those ascii chars.
const customFontLetterTables: {
  [scriptName: string]: { [key: string]: string }
} = {
  protosemitic: {
    א: "A",
    ב: "B",
    ג: "g",
    ד: "D",
    ה: "e",
    ו: "w",
    ז: "z",
    ח: "h",
    ט: "T",
    י: "y",
    כ: "k",
    ך: "k",
    ל: "l",
    מ: "m",
    ם: "m",
    נ: "n",
    ן: "n",
    ס: "o",
    ע: "s",
    פ: "p",
    ף: "p",
    צ: "X",
    ץ: "X",
    ק: "Q",
    ר: "R",
    ש: "S",
    ת: "t",
  },
  phoenician: {
    א: "a",
    ב: "b",
    ג: "g",
    ד: "d",
    ה: "h",
    ו: "w",
    ז: "i",
    ח: "H",
    ט: "T",
    י: "y",
    כ: "k",
    ך: "k",
    ל: "l",
    מ: "m",
    ם: "m",
    נ: "n",
    ן: "n",
    ס: "s",
    ע: "o",
    פ: "p",
    ף: "p",
    צ: "x",
    ץ: "x",
    ק: "q",
    ר: "r",
    ש: "S",
    ת: "t",
  },
  obry: {
    א: "a",
    ב: "b",
    ג: "g",
    ד: "d",
    ה: "e",
    ו: "u",
    ז: "z",
    ח: "h",
    ט: "t",
    י: "y",
    כ: "k",
    ך: "k",
    ל: "l",
    מ: "m",
    ם: "m",
    נ: "n",
    ן: "n",
    ס: "s",
    ע: "o",
    פ: "p",
    ף: "p",
    צ: "x",
    ץ: "x",
    ק: "q",
    ר: "r",
    ש: "w",
    ת: "i",
  },
}

export function isScriptLTR(displayScript: ScriptName): boolean {
  switch (displayScript) {
    case "hebrew":
      return false
    case "masoretic":
      return false
    default:
      return true
  }
}

export function buildWordDisplayText(
  source: string,
  settings: Settings,
): string {
  const displayScript = settings.displayScript
  switch (displayScript) {
    // Hebrew text uses the graphemes of the source text, without diacritics.
    case "hebrew": {
      return source.slice().replace(/[^אבגדהוזחטיכךלמםנןסעפףצץרקשת ]/g, "")
    }
    // Masoretic text includes all diacritics of the exact source text we use.
    // However, the source we have uses slashes sometimes that we want to omit.
    case "masoretic": {
      return source.slice().replace(/\//g, "")
    }
    // Transliterated matres lectionis is the same as our internal word id.
    case "tml": {
      return transliterateHebrewWord(source)
    }
    // Other texts uses certain ascii letters in that particular custom font.
    default: {
      return source.slice().replace(/./g, (grapheme) => {
        if (grapheme === " ") return " "
        return customFontLetterTables[displayScript][grapheme] || ""
      })
    }
  }
}

export function buildWordDisplayStyle(
  baseFontSize: number,
  settings: Settings,
) {
  const smallerFontSize = Math.round(baseFontSize * 0.867)
  switch (settings.displayScript) {
    case "protosemitic":
      return { fontSize: smallerFontSize, fontFamily: "protosemitic" }
    case "phoenician":
      return { fontSize: smallerFontSize, fontFamily: "phoenician" }
    case "obry":
      return { fontSize: smallerFontSize, fontFamily: "obry" }
    case "tml":
      return { fontSize: smallerFontSize }
    default:
      return { fontSize: baseFontSize }
  }
}

export function buildWordDisplayStyleString(
  baseFontSize: number,
  settings: Settings,
) {
  const smallerFontSize = Math.round(baseFontSize * 0.867)
  switch (settings.displayScript) {
    case "protosemitic":
      return `font-size: ${smallerFontSize}pt; font-family: 'protosemitic'`
    case "phoenician":
      return `font-size: ${smallerFontSize}pt; font-family: 'phoenician'`
    case "obry":
      return `font-size: ${smallerFontSize}pt; font-family: 'obry'`
    case "tml":
      return `font-size: ${smallerFontSize}pt`
    default:
      return `font-size: ${baseFontSize}pt`
  }
}
