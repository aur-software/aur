import {
  buildVerseName,
  downloadAndParseBook,
  untransliterateToHebrewWord,
} from "../SourceText"

test("buildVerseName", () => {
  expect(buildVerseName("gen.041.033")).toBe("Genesis 41:33")
})

test("downloadAndParseBook: wlc gen", async () => {
  const book = await downloadAndParseBook("wlc", "gen")

  expect(book.name).toBe("Genesis")
  expect(book.source).toBe("בְּרֵאשִׁית")
  expect(book.chapters.length).toBe(50)
  expect(book.chapters[40].id).toBe("gen.041")
  expect(book.chapters[40].name).toBe("Genesis 41")
  expect(book.chapters[40].source).toBe("בְּרֵאשִׁית 41")
  expect(book.chapters[40].verses[32].id).toBe("gen.041.033")
  expect(book.chapters[40].verses[32].name).toBe("Genesis 41:33")
  expect(book.chapters[40].verses[32].source).toBe("בְּרֵאשִׁית 41:33")
  expect(book.chapters[0].verses[0].words[0].id).toBe("gen.001.001.001")
  // expect(book.chapters[0].verses[0].words[0].ref).toBe("01xeN")
  expect(book.chapters[0].verses[0].words[0].source).toBe("בְּ/רֵאשִׁ֖ית")
  expect(book.chapters[0].verses[0].words[0].word).toBe("BRAShYT")
})

test("downloadAndParseBookMC: mc gen", async () => {
  const book = await downloadAndParseBook("mc", "gen")

  expect(book.name).toBe("Genesis")
  expect(book.source).toBe("בְּרֵאשִׁית")
  expect(book.chapters.length).toBe(50)
  expect(book.chapters[40].id).toBe("gen.041")
  expect(book.chapters[40].name).toBe("Genesis 41")
  expect(book.chapters[40].source).toBe("בְּרֵאשִׁית 41")
  expect(book.chapters[40].verses[32].id).toBe("gen.041.033")
  expect(book.chapters[40].verses[32].name).toBe("Genesis 41:33")
  expect(book.chapters[40].verses[32].source).toBe("בְּרֵאשִׁית 41:33")
  expect(book.chapters[0].verses[0].words[0].id).toBe("gen.001.001.001")
  // expect(book.chapters[0].verses[0].words[0].ref).toBe("01xeN")
  expect(book.chapters[0].verses[0].words[0].source).toBe("בְּרֵאשִׁית")
  expect(book.chapters[0].verses[0].words[0].word).toBe("BRAShYT")
})

test("untransliterateToHebrewWord: BRAShYT", () => {
  expect(untransliterateToHebrewWord("BRAShYT")).toBe("בראשית")
})

test("untransliterateToHebrewWord: MYM", () => {
  expect(untransliterateToHebrewWord("MYM")).toBe("מים")
})

test("untransliterateToHebrewWord: NUN", () => {
  expect(untransliterateToHebrewWord("NUN")).toBe("נון")
})
