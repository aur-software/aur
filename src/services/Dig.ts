import { Except } from "type-fest"

export type DigResult = {
  root: string
  ops: DigOp[]
}

export type DigOp = {
  id: string
  uid: number
  chipName?: string
  chipGlyphs?: string
  prefix?: string
  midfixHead?: string
  midfixHead2?: string
  midfixHeadPattern?: RegExp
  midfixTail?: string
  midfixTailPattern?: RegExp
  suffix?: string
  infixPattern?: RegExp
  checkPattern?: RegExp
  swappedPrefixForPrefix?: [string, string]
  droppedPrefixBehindPrefix?: string
  droppedPrefixBehindSamePrefix?: string
  droppedSuffix?: string
  droppedSuffixHeadTail?: string
  droppedSuffixBehindSuffix?: string
  droppedSuffixDouble?: boolean
  droppedMidfixHead?: string
  droppedMidfixInBiglyph?: string
  preventWeakBiToTriOpsUnlessMultiVerb?: boolean
  forbidFormerOps?: string[]
  requireLaterOp?: string
  requireFormerOp?: string
  requireLaterVerbOp?: boolean
  requireFormerVerbOp?: boolean
  uniToTri?: boolean
  biToTri?: boolean
  verb?: boolean
  allowEmptyAfterPossess?: boolean
  maximumPreApplySize?: number
  possess?: boolean
  preventParticleOrProper?: boolean
}
type DigOpNoId = Except<DigOp, "id" | "uid">

type DigState = {
  word: string
  stack: DigOp[]
  results: any[]
}
type DigStateFn = (state: DigState) => DigState
function stateNew(word: string): DigState {
  return {
    word: word,
    stack: [],
    results: [{ root: word, ops: [] }],
  }
}
function statePushOp(state: DigState, op: DigOp) {
  const modifiedWord = applyOpToWord(state.word, op)
  const modifiedStack = [...state.stack, op]
  const modifiedWordGlyphCount = glyphCount(modifiedWord)

  let results = state.results.slice()
  let recordResult = true

  // Don't record verb results unless the proposed root has exactly 3 glyphs.
  if (modifiedStack.some((op) => "verb" in op) && modifiedWordGlyphCount !== 3)
    recordResult = false

  // Don't record if the proposed root has just one glyph (or zero glyphs),
  // unless we have rules that explicitly allow that.
  const allowEmpty =
    modifiedWordGlyphCount === 0 &&
    modifiedStack.some((op) => "allowEmptyAfterPossess" in op) &&
    modifiedStack.some((op) => "possess" in op)
  if (modifiedWordGlyphCount < 2 && !allowEmpty) recordResult = false

  // Don't record if a required later rule never happened.
  modifiedStack.forEach((op, index) => {
    const remainingStack = modifiedStack.slice(index + 1)
    if ("requireLaterOp" in op) {
      if (!remainingStack.some((otherOp) => op.requireLaterOp === otherOp.id)) {
        recordResult = false
      }
    }
    if ("requireLaterVerbOp" in op) {
      if (!remainingStack.some((otherOp) => otherOp.verb)) {
        recordResult = false
      }
    }
  })

  // Don't record if a weak bi to tri op happened after it wasn't supposed to.
  modifiedStack.forEach((op) => {
    if (op.preventWeakBiToTriOpsUnlessMultiVerb) {
      if (
        modifiedStack.some(
          (otherOp) => otherOp.id.startsWith("Weak") && otherOp.biToTri,
        ) &&
        modifiedStack.filter((op) => op.verb).length <= 1
      ) {
        recordResult = false
      }
    }
  })

  // Record the result if it is valid.
  if (recordResult) results.push({ root: modifiedWord, ops: modifiedStack })

  return {
    word: modifiedWord,
    stack: modifiedStack,
    results: results,
  }
}

export const Ops: { [key: string]: DigOp } = {}
export const OpSets: { [key: string]: DigOp[] } = {}

function defOp(id: string, uid: number, opNoId: DigOpNoId): DigOp {
  const op: DigOp = { id, uid, ...opNoId }

  // Some operations cannot be applied to a particle or proper name.
  if (
    op.verb ||
    op.droppedPrefixBehindPrefix ||
    op.droppedPrefixBehindSamePrefix ||
    op.droppedSuffix ||
    op.droppedSuffixHeadTail ||
    op.droppedSuffixBehindSuffix ||
    op.droppedSuffixDouble ||
    op.droppedMidfixInBiglyph
  )
    op.preventParticleOrProper = true

  if (op.midfixHead)
    op.midfixHeadPattern = new RegExp(
      `^(${op.prefix ?? ""}[A-Z][a-z]?)${op.midfixHead}(?![a-z])`,
    )
  if (op.midfixHead2)
    op.midfixHeadPattern = new RegExp(
      `^(${op.prefix ?? ""}[A-Z][a-z]?[A-Z][a-z]?)${op.midfixHead2}(?![a-z])`,
    )
  if (op.midfixTail)
    op.midfixTailPattern = new RegExp(
      `${op.midfixTail}([A-Z][a-z]?${op.suffix ?? ""})$`,
    )

  if (id.startsWith("Consec")) {
    op.chipName = "Consecutive"
    op.chipGlyphs = id.replace("Consec", "")
  } else if (id.startsWith("Def")) {
    op.chipName = "Definite"
    op.chipGlyphs = id.replace("Def", "")
  } else if (id.startsWith("Prep")) {
    op.chipName = "Preposition"
    op.chipGlyphs = id.replace("Prep", "")
  } else if (id.startsWith("Plural")) {
    op.chipName = "Plural"
    op.chipGlyphs = id.replace(/Plural(Of)?/, "")
  } else if (id.startsWith("Possess")) {
    op.chipName = id
    op.chipGlyphs =
      (op.prefix || "") +
      (op.midfixHead || "") +
      (op.midfixHead2 || "") +
      (op.midfixTail || "") +
      (op.suffix || "")
  } else if (id.startsWith("Reflex")) {
    op.chipName = "Reflexive"
    op.chipGlyphs = id.replace("Reflex", "")
  } else if (id.startsWith("Cause")) {
    op.chipName = "Causative"
    op.chipGlyphs = id.replace("Cause", "")
  } else if (id.startsWith("Passive")) {
    op.chipName = "Passive"
    op.chipGlyphs = id.replace("Passive", "")
  } else if (id.startsWith("Inf")) {
    op.chipName = "Infinitive"
    op.chipGlyphs =
      (op.prefix || "") +
      (op.midfixHead || "") +
      (op.midfixHead2 || "") +
      (op.midfixTail || "") +
      (op.suffix || "")
  } else if (id.startsWith("ParticipleReflex")) {
    op.chipName = "ParticipleReflexive"
    op.chipGlyphs =
      (op.prefix || "") +
      (op.midfixHead || "") +
      (op.midfixHead2 || "") +
      (op.midfixTail || "") +
      (op.suffix || "")
  } else if (id.startsWith("ParticipleCause")) {
    op.chipName = "ParticipleCausative"
    op.chipGlyphs =
      (op.prefix || "") +
      (op.midfixHead || "") +
      (op.midfixHead2 || "") +
      (op.midfixTail || "") +
      (op.suffix || "")
  } else if (id.startsWith("Participle")) {
    op.chipName = "Participle"
    op.chipGlyphs =
      (op.prefix || "") +
      (op.midfixHead || "") +
      (op.midfixHead2 || "") +
      (op.midfixTail || "") +
      (op.suffix || "")
  } else if (id.startsWith("Perf")) {
    op.chipName = id.replace("Perf", "Perfect")
    op.chipGlyphs =
      (op.prefix || "") +
      (op.midfixHead || "") +
      (op.midfixHead2 || "") +
      (op.midfixTail || "") +
      (op.suffix || "")
  } else if (id.startsWith("Imperf")) {
    op.chipName = id.replace("Imperf", "Imperfect")
    op.chipGlyphs =
      (op.prefix || "") +
      (op.midfixHead || "") +
      (op.midfixHead2 || "") +
      (op.midfixTail || "") +
      (op.suffix || "")
  } else if (id.startsWith("Imperative")) {
    op.chipName = id
    op.chipGlyphs =
      (op.prefix || "") +
      (op.midfixHead || "") +
      (op.midfixHead2 || "") +
      (op.midfixTail || "") +
      (op.suffix || "")
  } else if (id.startsWith("Cohort")) {
    op.chipName = id.replace("Cohort", "Cohortative")
    op.chipGlyphs =
      (op.prefix || "") +
      (op.midfixHead || "") +
      (op.midfixHead2 || "") +
      (op.midfixTail || "") +
      (op.suffix || "")
  } else if (id.startsWith("WeakDouble")) {
    op.chipName = "WeakDouble"
    op.chipGlyphs = ""
  } else if (id.startsWith("Weak")) {
    op.chipName = "WeakGlyph"
    op.chipGlyphs = id.replace(/Weak(Start|Final)?|[0-9].+/g, "")
  } else if (id.startsWith("Approx")) {
    op.chipName = "Approximate"
    op.chipGlyphs =
      (op.prefix || "") +
      (op.midfixHead || "") +
      (op.midfixHead2 || "") +
      (op.midfixTail || "") +
      (op.suffix || "")
  } else if (id.startsWith("Solidify")) {
    op.chipName = "Solidify"
    op.chipGlyphs =
      (op.prefix || "") +
      (op.midfixHead || "") +
      (op.midfixHead2 || "") +
      (op.midfixTail || "") +
      (op.suffix || "")
  } else if (id.startsWith("SolidifyU") || id.startsWith("ParticiplePassive")) {
    op.chipName = "Participle"
    op.chipGlyphs =
      (op.prefix || "") +
      (op.midfixHead || "") +
      (op.midfixHead2 || "") +
      (op.midfixTail || "") +
      (op.suffix || "")
  }

  return (Ops[id] = op)
}
function defVerb(id: string, uid: number, op: DigOpNoId): DigOp {
  return defOp(id, uid, { ...op, verb: true })
}
function defVerbMod(id: string, uid: number, op: DigOpNoId): DigOp {
  return defOp(id, uid, { ...op, verb: true, requireLaterVerbOp: true })
}
function defOpSet(id: string, ops: (DigOp | DigOp[])[]): DigOp[] {
  return (OpSets[id] = ops.flatMap((op) => (op instanceof Array ? op : [op])))
}

export function digWord(word: string): DigResult[] {
  return digWordInner(stateNew(word)).results
}

defOp("ConsecU", 1, { prefix: "U" })

defOpSet("Prep", [
  defOp("PrepB", 2, { prefix: "B", allowEmptyAfterPossess: true }),
  defOp("PrepK", 3, { prefix: "K", allowEmptyAfterPossess: true }),
  defOp("PrepL", 4, { prefix: "L", allowEmptyAfterPossess: true }),
  defOp("PrepM", 5, { prefix: "M", allowEmptyAfterPossess: true }),
])

defOp("DefE", 6, { prefix: "E" })

defOpSet("Plural", [
  defOp("PluralYM", 7, { suffix: "YM" }),
  defOp("PluralUT", 8, { suffix: "UT", droppedSuffix: "E" }),
])

defOpSet("PluralOf", [
  defOp("PluralOfY", 9, { suffix: "Y", forbidFormerOps: ["Possess3mp"] }),
  defOp("PluralOfUTY", 10, {
    suffix: "UTY",
    droppedSuffix: "E",
    forbidFormerOps: ["Possess3mp"],
  }),
  defOp("PluralOfTY", 11, {
    suffix: "TY",
    droppedSuffix: "E",
    forbidFormerOps: ["Possess3mp"],
  }),
  defOp("PluralOfT", 12, { suffix: "T", droppedSuffix: "E" }),
])

defOpSet("Possess", [
  defOp("Possess3ms", 13, { suffix: "U", possess: true }),
  defOp("Possess3fs", 14, { suffix: "E", possess: true }),
  defOp("Possess3fsYE", 15, {
    suffix: "YE",
    droppedSuffix: "E",
    possess: true,
  }),
  defOp("Possess2cs", 16, { suffix: "K", possess: true }),
  defOp("Possess1cs", 17, { suffix: "Y", possess: true }),
  defOp("Possess3mp", 18, { suffix: "M", possess: true }),
  defOp("Possess3mpE", 19, { suffix: "EM", possess: true }),
  defOp("Possess3fp", 20, { suffix: "N", possess: true }),
  defOp("Possess2mp", 21, { suffix: "KM", possess: true }),
  defOp("Possess2fp", 22, { suffix: "KN", possess: true }),
  defOp("Possess1cp", 23, { suffix: "NU", possess: true }),
])

defOpSet("Approx", [
  defOp("ApproxE", 24, { suffix: "E" }),
  defOp("ApproxT", 132, {
    suffix: "TE",
    droppedSuffix: "E",
    forbidFormerOps: ["Possess3fsYE", "PluralOfTY"],
  }),
])

defOpSet("Solidify", [
  defOp("SolidifyM", 25, { prefix: "M" }),
  defOp("SolidifyN", 136, { prefix: "N" }),
  defOp("SolidifyY", 26, { midfixTail: "Y", verb: true }), // TODO: rename `verb` as `require3`? this isn't really a verb...
  defOp("SolidifyTShUE", 130, {
    prefix: "T",
    suffix: "E",
    checkPattern: /^TShU[A-Z][a-z]?E$/, // TODO: is there any evidence for this pattern on other roots?
  }),
  defOp("SolidifyU12", 27, { midfixHead: "U", verb: true }),
  defOp("SolidifyU23", 28, { midfixTail: "U", verb: true }),
])

defOpSet("VerbModifierImperf", [
  defVerbMod("ReflexT", 29, { midfixHead: "T" }),
  defVerbMod("ReflexShT", 30, {
    checkPattern: /^[A-Z][a-z]?Sh/,
    midfixHead2: "T",
  }),
])

defOpSet("VerbModifierPerf", [defVerbMod("PassiveN", 31, { prefix: "N" })])

defOpSet("VerbModifierPerfOrImperative", [
  defVerbMod("ReflexET", 32, { prefix: "ET" }),
  defVerbMod("CauseE", 33, { prefix: "E" }),
])

defOpSet("VerbModifier", [
  defVerbMod("CauseEY", 34, {
    prefix: "E",
    checkPattern: /[A-Z][a-z]?Y[A-Z][a-z]?/,
    requireLaterOp: "FinCauseEY",
  }),
  defVerbMod("CauseY", 35, {
    checkPattern: /[A-Z][a-z]?Y[A-Z][a-z]?/,
    requireLaterOp: "FinCauseY",
  }),
])

defOpSet("FinCause", [
  defOp("FinCauseEY", 36, { midfixTail: "Y", requireFormerOp: "CauseEY" }),
  defOp("FinCauseY", 37, { midfixTail: "Y", requireFormerOp: "CauseY" }),
])

defOpSet("VerbParticiple", [
  defVerb("Participle3ms", 38, { forbidFormerOps: ["PrepL"], prefix: "L" }),
  defVerb("Participle3fs", 39, {
    forbidFormerOps: ["PrepL"],
    prefix: "L",
    suffix: "T",
  }),
  defVerb("Participlemp", 40, { prefix: "M", suffix: "YM" }),
  defVerb("Participlefp", 41, { prefix: "M", suffix: "UT" }),
  defVerb("ParticiplePassivemp", 42, { prefix: "N", suffix: "YM" }),
  defVerb("ParticiplePassivefp", 43, { prefix: "N", suffix: "UT" }),
  defVerb("ParticipleReflex1ms", 44, { prefix: "MT" }),
  defVerb("ParticipleReflex1fs", 45, { prefix: "MT", suffix: "T" }),
  defVerb("ParticipleReflex1mp", 46, { prefix: "MT", suffix: "YM" }),
  defVerb("ParticipleReflex1fp", 47, { prefix: "MT", suffix: "UT" }),
  defVerb("ParticipleReflex3cs", 48, {
    forbidFormerOps: ["PrepL"],
    prefix: "LET",
  }),
  defVerb("ParticipleCause1ms", 49, { prefix: "M", midfixTail: "Y" }),
  defVerb("ParticipleCause1fs", 50, { prefix: "M", suffix: "T" }),
  defVerb("ParticipleCause1mp", 51, {
    prefix: "M",
    midfixTail: "Y",
    suffix: "YM",
  }),
  defVerb("ParticipleCause1fp", 52, {
    prefix: "M",
    midfixTail: "Y",
    suffix: "UT",
  }),
  defVerb("ParticipleCause3ms", 53, {
    forbidFormerOps: ["PrepL"],
    prefix: "LE",
    midfixTail: "Y?",
  }),
  defVerb("ParticipleCause3fs", 54, {
    forbidFormerOps: ["PrepL"],
    prefix: "LE",
    suffix: "T",
  }),
  defVerb("ParticipleCause3mp", 55, {
    forbidFormerOps: ["PrepL"],
    prefix: "LE",
    midfixTail: "Y?",
    suffix: "YM",
  }),
  defVerb("ParticipleCause3fp", 56, {
    forbidFormerOps: ["PrepL"],
    prefix: "LE",
    midfixTail: "Y?",
    suffix: "UT",
  }),
])

defOpSet("VerbSimple", [
  defVerb("InfAbs", 57, { midfixTail: "U" }),

  defVerb("InfCause", 58, {
    // This conjugation has no glyph indicators, so we add some constraints
    // to prevent some frivolous suggestions of this interpretation.
    requireFormerVerbOp: true,
    preventWeakBiToTriOpsUnlessMultiVerb: true,
    requireLaterOp: "FinCauseEY",
  }),

  defOpSet("VerbImperf", [
    defVerb("Imperf3ms", 59, { prefix: "Y" }),
    defVerb("Imperf3msTo3fs", 60, { prefix: "Y", suffix: "E" }),
    defVerb("Imperf3msTo3mp", 61, { prefix: "Y", suffix: "M" }),
    defVerb("Imperf3msTo1cp", 62, { prefix: "Y", suffix: "NU" }),
    defVerb("Imperf2ms", 63, { prefix: "T" }),
    defVerb("Imperf3fs", 64, { prefix: "T" }),
    defVerb("Imperf3fsTo3ms", 65, { prefix: "T", suffix: "EU" }),
    defVerb("Imperf3fsTo1cs", 137, { prefix: "T", suffix: "NY" }),
    defVerb("Imperf2fs", 66, { prefix: "T", suffix: "Y" }),
    defVerb("Imperf2fsN", 67, { prefix: "T", suffix: "YN" }),
    defVerb("Imperf1cs", 68, { prefix: "A" }),
    defVerb("Imperf1csTo2cs", 140, { prefix: "A", suffix: "K" }),
    defVerb("Imperf1csTo3fs", 141, { prefix: "A", suffix: "E" }),
    defVerb("Imperf1csTo3mp", 69, { prefix: "A", suffix: "M" }),
    defVerb("Imperf3mp", 70, { prefix: "Y", suffix: "U" }),
    defVerb("Imperf3mpN", 71, { prefix: "Y", suffix: "UN" }),
    defVerb("Imperf3mpTo1cs", 72, { prefix: "Y", suffix: "NY" }),
    defVerb("Imperf3mpTo2cs", 73, { prefix: "Y", suffix: "UKE" }),
    defVerb("Imperf3mpTo3ms", 134, { prefix: "Y", suffix: "EU" }),
    defVerb("Imperf3mpTo3mso", 74, { prefix: "Y", suffix: "UEU" }),
    defVerb("Imperf3mpTo3fs", 142, { prefix: "Y", suffix: "UE" }),
    defVerb("Imperf3mpTo3mp", 75, { prefix: "Y", suffix: "UM" }),
    defVerb("Imperf2mp", 76, { prefix: "T", suffix: "U" }),
    defVerb("Imperf2mpN", 77, { prefix: "T", suffix: "UN" }),
    defVerb("Imperf3fp", 78, { prefix: "T", suffix: "NE" }),
    defVerb("Imperf3fpo", 138, { prefix: "T", suffix: "N" }),
    defVerb("Imperf2fp", 79, { prefix: "T", suffix: "NE" }),
    defVerb("Imperf2fpo", 139, { prefix: "T", suffix: "N" }),
    defVerb("Imperf1cp", 80, { prefix: "N" }),
  ]),

  defOpSet("VerbPerf", [
    defVerb("Perf3ms", 81, {}),
    defVerb("Perf3msTo1cs", 82, { suffix: "NY" }),
    defVerb("Perf2cs", 83, { suffix: "T" }),
    defVerb("Perf2cso", 84, { suffix: "TE" }),
    defVerb("Perf2csTo1cs", 85, { suffix: "TNY" }),
    defVerb("Perf2csTo3ms", 86, { suffix: "TU" }),
    defVerb("Perf3fs", 87, { suffix: "E" }),
    defVerb("Perf3fsTo3ms", 88, { suffix: "TEU" }),
    defVerb("Perf3fsTo3mso", 89, { suffix: "TU" }),
    defVerb("Perf3fsTo3fs", 90, { suffix: "TE" }),
    defVerb("Perf1cs", 91, { suffix: "TY" }),
    defVerb("Perf1csTo2cs", 92, { suffix: "TYK" }),
    defVerb("Perf1csTo3ms", 93, { suffix: "TYU" }),
    defVerb("Perf1csTo3fs", 94, { suffix: "TYE" }),
    defVerb("Perf1csTo3mp", 95, { suffix: "TYM" }),
    defVerb("Perf3cp", 96, { suffix: "U" }),
    defVerb("Perf3cpTo1cs", 97, { suffix: "UNY" }),
    defVerb("Perf3cpTo2ms", 133, { suffix: "UK" }),
    defVerb("Perf2mp", 98, { suffix: "TM" }),
    defVerb("Perf2fp", 99, { suffix: "TN" }),
    defVerb("Perf1cp", 100, { suffix: "NU" }),
    defVerb("Perf1cpo", 101, { suffix: "TNU" }),
    defVerb("Perf1cpWeak3E", 102, { droppedSuffix: "NE", suffix: "NU" }),
  ]),

  defOpSet("VerbImperative", [
    defVerb("Imperative2ms", 103, {}),
    defVerb("ImperativeTo1cs", 104, { suffix: "NY" }),
    defVerb("ImperativeTo1cp", 105, { suffix: "NU" }),
    defVerb("Imperative2fs", 106, { suffix: "Y" }),
    defVerb("Imperative2fso", 107, { prefix: "Y", suffix: "Y" }),
    defVerb("Imperative2mp", 108, { suffix: "U" }),
    defVerb("Imperative2mpo", 109, { prefix: "Y", suffix: "U" }),
    defVerb("Imperative2fp", 110, { suffix: "NE" }),
  ]),

  defOpSet("VerbCohort", [
    defVerb("Cohort1cs", 111, { prefix: "A", suffix: "E" }),
    defVerb("Cohort1cp", 112, { prefix: "N", suffix: "E" }),
  ]),
])

OpSets.VerbPerfOrImperative = [...OpSets.VerbPerf, ...OpSets.VerbImperative]

defOpSet("Weak", [
  defOp("WeakE1", 113, { biToTri: true, droppedPrefixBehindPrefix: "E" }),
  defOp("WeakN1", 114, { biToTri: true, droppedPrefixBehindPrefix: "N" }),
  defOp("WeakL1", 115, { biToTri: true, droppedPrefixBehindPrefix: "L" }),
  defOp("WeakY1", 116, { biToTri: true, droppedPrefixBehindPrefix: "Y" }),
  defOp("WeakY1U", 117, { swappedPrefixForPrefix: ["Y", "U"] }),
  defOp("WeakA1", 118, { biToTri: true, droppedPrefixBehindSamePrefix: "A" }),
  defOp("WeakU2", 119, { biToTri: true, droppedMidfixHead: "U" }),
  defOp("WeakY2", 120, { biToTri: true, droppedMidfixHead: "Y" }),
  defOp("WeakE3T", 121, { droppedSuffix: "E", suffix: "T" }),
  defOp("WeakE3Y", 122, { droppedSuffix: "E", suffix: "Y" }),
  defOp("WeakE3", 123, { biToTri: true, droppedSuffix: "E" }),
  defOp("WeakY3", 124, { biToTri: true, droppedSuffix: "Y" }),
  defOp("WeakN3", 125, { biToTri: true, droppedSuffixHeadTail: "N" }),
  defOp("WeakDouble", 126, { biToTri: true, droppedSuffixDouble: true }),
  defOp("WeakN1E3", 131, {
    uniToTri: true,
    droppedPrefixBehindPrefix: "N",
    droppedSuffixBehindSuffix: "E",
  }),
  defOp("WeakY1UE3", 135, {
    biToTri: true,
    swappedPrefixForPrefix: ["Y", "U"],
    droppedSuffix: "E",
  }),
])

defOpSet("WeakNoun", [
  defOp("WeakInitN", 127, {
    droppedPrefixBehindPrefix: "N",
    maximumPreApplySize: 2,
    forbidFormerOps: ["SolidifyN"],
  }),
  defOp("WeakMidU", 128, {
    droppedMidfixInBiglyph: "U",
    maximumPreApplySize: 2,
    forbidFormerOps: ["SolidifyU12", "SolidifyU23"],
  }),
  defOp("WeakFinalE", 129, {
    droppedSuffixBehindSuffix: "E",
    maximumPreApplySize: 2,
    forbidFormerOps: [
      "ApproxE",
      "ApproxT",
      "Possess3fs",
      "Possess3fsYE",
      "PluralOfT",
      "SolidifyU12",
      "SolidifyU23",
    ],
  }),
])

const finishNoun = (state: DigState) => maybeAnyOf(state, OpSets.WeakNoun)

const finishVerb = (state: DigState) =>
  maybeAnyOf(state, OpSets.Weak, (state) => maybeAnyOf(state, OpSets.FinCause))

function digWordInner(state: DigState): DigState {
  return maybe(state, Ops.ConsecU, (state) =>
    maybeAnyOfOrNoneOrElse(
      state,
      OpSets.Prep,
      (state) =>
        maybe(state, Ops.DefE, (state) =>
          maybeAnyOfOrElse(state, OpSets.PluralOf, finishNoun, (state) =>
            maybeAnyOfOrNoneOrElse(state, OpSets.Possess, (state) =>
              maybeAnyOfOrElse(
                state,
                OpSets.VerbParticiple,
                finishVerb,
                (state) =>
                  maybeAnyOf(
                    state,
                    [...OpSets.PluralOf, ...OpSets.Plural],
                    (state) =>
                      maybeAnyOf(state, OpSets.Approx, (state) =>
                        maybeAnyOf(state, OpSets.Solidify, finishNoun),
                      ),
                  ),
              ),
            ),
          ),
        ),

      (state) =>
        maybeAnyOfOrElse(
          state,
          OpSets.VerbModifierImperf,
          (state) => anyOf(state, OpSets.VerbImperf, finishVerb),
          (state) =>
            maybeAnyOfOrElse(
              state,
              OpSets.VerbModifierPerf,
              (state) => anyOf(state, OpSets.VerbPerf, finishVerb),
              (state) =>
                maybeAnyOfOrElse(
                  state,
                  OpSets.VerbModifierPerfOrImperative,
                  (state) =>
                    anyOf(state, OpSets.VerbPerfOrImperative, finishVerb),
                  (state) =>
                    maybeAnyOf(state, OpSets.VerbModifier, (state) =>
                      anyOf(state, OpSets.VerbSimple, finishVerb),
                    ),
                ),
            ),
        ),
    ),
  )
}

function maybe(
  state: DigState,
  op: DigOp,
  nextFn: DigStateFn = (state) => state,
): DigState {
  return maybeAnyOf(state, [op], nextFn)
}

function maybeAnyOf(
  state: DigState,
  ops: DigOp[],
  nextFn: DigStateFn = (state) => state,
) {
  return maybeAnyOfOrElse(state, ops, nextFn, nextFn)
}

function anyOf(
  state: DigState,
  ops: DigOp[],
  nextFn: DigStateFn = (state) => state,
) {
  return maybeAnyOfOrElse(state, ops, nextFn, (state) => state)
}

function maybeAnyOfOrElse(
  state: DigState,
  ops: DigOp[],
  trueFn: DigStateFn = (state) => state,
  elseFn: DigStateFn = (state) => state,
): DigState {
  const matchedStates: DigState[] = []
  ops.forEach((op) => {
    if (matchOp(state, op)) {
      matchedStates.push(trueFn(statePushOp(state, op)))
    }
  })
  matchedStates.push(elseFn(state))

  return {
    ...state,
    results: [
      ...state.results,
      ...matchedStates.flatMap((s) => s.results.slice(state.results.length)),
    ],
  }
}

function maybeAnyOfOrNoneOrElse(
  state: DigState,
  ops: DigOp[],
  trueFn: DigStateFn = (state) => state,
  elseFn: DigStateFn = (state) => state,
): DigState {
  const matchedStates: DigState[] = []
  ops.forEach((op) => {
    if (matchOp(state, op)) {
      matchedStates.push(trueFn(statePushOp(state, op)))
    }
  })
  matchedStates.push(trueFn(state))
  matchedStates.push(elseFn(state))

  return {
    ...state,
    results: [
      ...state.results,
      ...matchedStates.flatMap((s) => s.results.slice(state.results.length)),
    ],
  }
}

function glyphCount(word: string) {
  return (word.match(/[A-Z][a-z]?/g) || []).length
}

function matchOp(state: DigState, op: DigOp): boolean {
  const word = state.word
  if ("prefix" in op) {
    if (
      op.prefix !== word.slice(0, op.prefix!.length) ||
      (word[op.prefix.length] || "").match(/[a-z]/)
    )
      return false
  }
  if ("midfixHeadPattern" in op) {
    if (!op.midfixHeadPattern!.test(word)) return false
  }
  if ("midfixTailPattern" in op) {
    if (!op.midfixTailPattern!.test(word)) return false
  }
  if ("suffix" in op) {
    if (op.suffix !== word.slice(-1 * op.suffix!.length)) return false
  }
  if ("infixPattern" in op) {
    if (!op.infixPattern!.test(word)) return false
  }
  if ("checkPattern" in op) {
    if (!op.checkPattern!.test(word)) return false
  }
  if ("swappedPrefixForPrefix" in op) {
    const prefix = op.swappedPrefixForPrefix?.[1]
    if (
      !prefix ||
      prefix !== word.slice(0, prefix.length) ||
      (word[prefix.length] || "").match(/[a-z]/)
    )
      return false
  }
  if ("droppedPrefixBehindPrefix" in op) {
    if (!state.stack.some((op) => "prefix" in op)) return false
  }
  if ("droppedPrefixBehindSamePrefix" in op) {
    const prefix = op.droppedPrefixBehindSamePrefix
    if (!state.stack.some((op) => op.prefix === prefix)) return false
  }
  if ("droppedMidfixInBiglyph" in op) {
    if (glyphCount(word) !== 2) return false
  }
  if ("droppedSuffixBehindSuffix" in op) {
    if (!state.stack.some((op) => "suffix" in op)) return false
  }
  if ("droppedSuffixHeadTail" in op && op.droppedSuffixHeadTail) {
    if (glyphCount(word) !== 2) return false
    if (!word.startsWith(op.droppedSuffixHeadTail)) return false
  }
  if ("droppedSuffixDouble" in op) {
    if (glyphCount(word) !== 2) return false
  }
  if ("forbidFormerOps" in op) {
    if (state.stack.some((o) => op.forbidFormerOps!.some((id) => o.id === id)))
      return false
  }
  if ("requireFormerOp" in op) {
    if (!state.stack.some((o) => o.id === op.requireFormerOp)) return false
  }
  if ("requireFormerVerbOp" in op) {
    if (!state.stack.some((o) => o.verb)) return false
  }
  if ("maximumPreApplySize" in op) {
    if (glyphCount(word) > op.maximumPreApplySize!) return false
  }
  return true
}

export function applyOpToWord(word: string, op: DigOp) {
  let modifiedWord = word

  if ("infixPattern" in op) {
    modifiedWord = modifiedWord.slice().replace(op.infixPattern!, "")
  }
  if ("prefix" in op) {
    modifiedWord = modifiedWord.slice(op.prefix!.length)
  }
  if ("midfixHeadPattern" in op) {
    modifiedWord = modifiedWord.slice().replace(op.midfixHeadPattern!, "$1")
  }
  if ("midfixTailPattern" in op) {
    modifiedWord = modifiedWord.slice().replace(op.midfixTailPattern!, "$1")
  }
  if ("suffix" in op) {
    modifiedWord = modifiedWord.slice(0, -1 * op.suffix!.length)
  }
  if ("swappedPrefixForPrefix" in op) {
    const [restorePrefix, prefix] = op.swappedPrefixForPrefix!
    modifiedWord = restorePrefix + modifiedWord.slice(prefix.length)
  }
  if ("droppedPrefixBehindPrefix" in op) {
    modifiedWord = op.droppedPrefixBehindPrefix + modifiedWord
  }
  if ("droppedPrefixBehindSamePrefix" in op) {
    modifiedWord = op.droppedPrefixBehindSamePrefix + modifiedWord
  }
  if ("droppedSuffix" in op) {
    modifiedWord = modifiedWord + op.droppedSuffix
  }
  if ("droppedSuffixHeadTail" in op) {
    modifiedWord = modifiedWord + op.droppedSuffixHeadTail
  }
  if ("droppedSuffixBehindSuffix" in op) {
    modifiedWord = modifiedWord + op.droppedSuffixBehindSuffix
  }
  if ("droppedSuffixDouble" in op) {
    modifiedWord = modifiedWord + modifiedWord.match(/[A-Z][a-z]?$/)![0]
  }
  if ("droppedMidfixHead" in op) {
    modifiedWord = modifiedWord
      .slice()
      .replace(/^([A-Z][a-z]?)/, `$1${op.droppedMidfixHead!}`)
  }
  if ("droppedMidfixInBiglyph" in op) {
    modifiedWord = modifiedWord
      .slice()
      .replace(
        /([A-Z][a-z]?)([A-Z][a-z]?)/,
        `$1${op.droppedMidfixInBiglyph!}$2`,
      )
  }

  return modifiedWord
}

export function summarizeOps(ops: DigOp[]) {
  const [preParts, postParts] = summarizeVerbOps(ops)

  var plural = false
  var pluralOf = false
  var approx = false
  var consec = false
  var definite = false
  const preps: string[] = []
  const possess: string[] = []
  const solidify: [string | null, string | null][] = []

  ops.forEach((op) => {
    switch (op.id) {
      case "PluralYM":
      case "PluralUT": {
        plural = true
        break
      }
      case "PluralOfY":
      case "PluralOfUTY":
      case "PluralOfTY":
      case "PluralOfT": {
        pluralOf = true
        break
      }
      case "SolidifyM": {
        solidify.push(["(something used for)", "(ing)"])
        break
      }
      case "SolidifyN": {
        solidify.push(["(something/one that has been)", "(d)"])
        break
      }
      case "SolidifyY": {
        solidify.push(["(something/one that)", "(s)"])
        break
      }
      case "SolidifyTShUE": {
        solidify.push(["(a response to)", "(ing)"])
        break
      }
      case "SolidifyU12": {
        solidify.push([null, "(er)"])
        break
      }
      case "SolidifyU23": {
        solidify.push([null, "(ing)"])
        break
      }
      case "ApproxE":
      case "ApproxT": {
        approx = true
        break
      }
      case "ConsecU": {
        consec = true
        break
      }
      case "DefE": {
        definite = true
        break
      }
      case "PrepB": {
        preps.push("(in)")
        break
      }
      case "PrepK": {
        preps.push("(like/as)")
        break
      }
      case "PrepL": {
        preps.push("(to)")
        break
      }
      case "PrepM": {
        preps.push("(from)")
        break
      }
      case "Possess3ms": {
        possess.push("(of him/it)")
        break
      }
      case "Possess3fs":
      case "Possess3fsYE": {
        possess.push("(of her/it)")
        break
      }
      case "Possess2cs": {
        possess.push("(of you)")
        break
      }
      case "Possess1cs": {
        possess.push("(of me)")
        break
      }
      case "Possess3mp": {
        possess.push("(of them)")
        break
      }
      case "Possess3mpE": {
        possess.push("(of them)")
        break
      }
      case "Possess3fp": {
        possess.push("(of them)")
        break
      }
      case "Possess2mp": {
        possess.push("(of you all)")
        break
      }
      case "Possess2fp": {
        possess.push("(of you all)")
        break
      }
      case "Possess1cp": {
        possess.push("(of us)")
        break
      }
    }
  })

  if (plural || pluralOf) postParts.push("(s)")

  if (pluralOf && possess.length === 0) postParts.push("(of)")

  possess.forEach((part) => postParts.push(part))

  solidify.forEach(([pre, post]) => {
    if (pre) preParts.push(pre)
    if (post) postParts.push(post)
  })

  if (approx) preParts.push("(something near)")

  if (definite) preParts.push("(the)")

  preps.forEach((part) => preParts.push(part))

  if (consec) preParts.push("(and)")

  return [preParts, postParts]
}

export function summarizeVerbOps(ops: DigOp[]) {
  const causative = ops.some(
    (op) => op.id.startsWith("Cause") || op.id.startsWith("ParticipleCause"),
  )
  const reflexive = ops.some((op) => op.id.startsWith("Reflex"))
  const passive = ops.some((op) => op.id.startsWith("Passive"))
  const lastVerb = ops
    .slice()
    .reverse()
    .find((op) => op.verb)

  if (causative) {
    switch (lastVerb?.id) {
      case "InfAbs":
        return [["(to cause to)"], []]
      case "InfCause":
        return [["(to cause to)"], []]
      case "ParticipleReflex1ms":
        return [["(causing self to be)"], ["(d)"]]
      case "ParticipleReflex1fs":
        return [["(causing self to be)"], ["(d)"]]
      case "ParticipleReflex1mp":
        return [["(causing selves to be)"], ["(d)"]]
      case "ParticipleReflex1fp":
        return [["(causing selves to be)"], ["(d)"]]
      case "ParticipleReflex3cs":
        return [["(causing him/her/itself to be)"], ["(d)"]]
      case "ParticipleCause1ms":
        return [["(my causing to)"], []]
      case "ParticipleCause1fs":
        return [["(my causing to)"], []]
      case "ParticipleCause1mp":
        return [["(our causing to)"], []]
      case "ParticipleCause1fp":
        return [["(our causing to)"], []]
      case "ParticipleCause3ms":
        return [["(his/its causing to)"], []]
      case "ParticipleCause3fs":
        return [["(her/its causing to)"], []]
      case "ParticipleCause3mp":
        return [["(their causing to)"], []]
      case "ParticipleCause3fp":
        return [["(their causing to)"], []]
      case "Imperf3ms":
        return [["(he/it causes to)"], []]
      case "Imperf3msTo3fs":
        return [["(he/it causes her/it to)"], []]
      case "Imperf3msTo3mp":
        return [["(he/it causes them to)"], []]
      case "Imperf3msTo1cp":
        return [["(he/it causes us to)"], []]
      case "Imperf3fs":
        return [["(she/it causes to)"], []]
      case "Imperf3fsTo3ms":
        return [["(she/it causes him/it to)"], []]
      case "Imperf3fsTo3fs":
        return [["(she/it causes her/it to)"], []]
      case "Imperf3fsTo1cs":
        return [["(she/it causes me to)"], []]
      case "Imperf2ms":
        return [["(you cause to)"], []]
      case "Imperf2fs":
      case "Imperf2fsN":
        return [["(you cause to)"], []]
      case "Imperf1cs":
        return [["(I cause to)"], []]
      case "Imperf1csTo2cs":
        return [["(I cause you to)"], []]
      case "Imperf1csTo3fs":
        return [["(I cause her/it to)"], []]
      case "Imperf1csTo3mp":
        return [["(I cause them to)"], []]
      case "Imperf3mp":
      case "Imperf3mpN":
        return [["(they cause to)"], []]
      case "Imperf3mpTo1cs":
        return [["(they cause me to)"], []]
      case "Imperf3mpTo2cs":
        return [["(they cause you to)"], []]
      case "Imperf3mpTo3ms":
      case "Imperf3mpTo3mso":
        return [["(they cause him/it to)"], []]
      case "Imperf3mpTo3fs":
        return [["(they cause her/it to)"], []]
      case "Imperf3mpTo3mp":
        return [["(they cause them to)"], []]
      case "Imperf3fp":
      case "Imperf3fpo":
        return [["(they cause to)"], []]
      case "Imperf2mp":
      case "Imperf2mpN":
        return [["(you all cause to)"], []]
      case "Imperf2fp":
      case "Imperf2fpo":
        return [["(you all cause to)"], []]
      case "Imperf1cp":
        return [["(we cause to)"], []]
      case "Perf3ms":
        return [["(he/it has caused to)"], []]
      case "Perf3msTo1cs":
        return [["(he/it has caused me to)"], []]
      case "Perf2cs":
      case "Perf2cso":
        return [["(you have caused to)"], []]
      case "Perf2csTo1cs":
        return [["(you have caused me to)"], []]
      case "Perf2csTo3ms":
        return [["(you have caused him/it to)"], []]
      case "Perf3fs":
        return [["(she/it has caused to)"], []]
      case "Perf3fsTo3ms":
      case "Perf3fsTo3mso":
        return [["(she/it has caused him/it to)"], []]
      case "Perf3fsTo3fs":
        return [["(she/it has caused her/it to)"], []]
      case "Perf1cs":
        return [["(I have caused to)"], []]
      case "Perf1csTo2cs":
        return [["(I have caused you to)"], []]
      case "Perf1csTo3ms":
        return [["(I have caused him/it to)"], []]
      case "Perf1csTo3fs":
        return [["(I have caused her/it to)"], []]
      case "Perf1csTo3mp":
        return [["(I have caused them to)"], []]
      case "Perf3cp":
        return [["(they have caused to)"], []]
      case "Perf3cpTo1cs":
        return [["(they have caused me to)"], []]
      case "Perf3cpTo2ms":
        return [["(they have caused you to)"], []]
      case "Perf2mp":
        return [["(you all have caused to)"], []]
      case "Perf2fp":
        return [["(you all have caused to)"], []]
      case "Perf1cp":
        return [["(we have caused to)"], []]
      case "Perf1cpo":
        return [["(we have caused to)"], []]
      case "Perf1cpWeak3E":
        return [["(we have caused to)"], []]
      case "ImperativeTo1cs":
        return [["(cause me to)"], ["(, you!)"]]
      case "ImperativeTo1cp":
        return [["(cause us to)"], ["(, you!)"]]
      case "Imperative2ms":
        return [["(cause to)"], ["(, you!)"]]
      case "Imperative2fs":
        return [["(cause to)"], ["(, you!)"]]
      case "Imperative2fso":
        return [["(cause to)"], ["(, you!)"]]
      case "Imperative2mp":
        return [["(cause to)"], ["(, you all!)"]]
      case "Imperative2mpo":
        return [["(cause to)"], ["(, you all!)"]]
      case "Imperative2fp":
        return [["(cause to)"], ["(, you all!)"]]
      case "Cohort1cs":
        return [["(let me cause to)"], []]
      case "Cohort1cp":
        return [["(let us cause to)"], []]
    }
  } else if (reflexive) {
    switch (lastVerb?.id) {
      case "InfAbs":
        return [["(to)"], ["(oneself)"]]
      case "InfCause":
        return [["(to cause to)"], ["(oneself)"]]
      case "ParticipleReflex1ms":
        return [[], ["(ing self)"]]
      case "ParticipleReflex1fs":
        return [[], ["(ing self)"]]
      case "ParticipleReflex1mp":
        return [[], ["(ing selves)"]]
      case "ParticipleReflex1fp":
        return [[], ["(ing selves)"]]
      case "ParticipleReflex3cs":
        return [[], ["(ing him/her/itself)"]]
      case "ParticipleCause1ms":
        return [["(causing self to be)"], ["(d)"]]
      case "ParticipleCause1fs":
        return [["(causing self to be)"], ["(d)"]]
      case "ParticipleCause1mp":
        return [["(causing selves to be)"], ["(d)"]]
      case "ParticipleCause1fp":
        return [["(causing selves to be)"], ["(d)"]]
      case "Imperf3ms":
        return [["(he/it)"], ["(s)", "(him/itself)"]]
      case "Imperf3msTo3fs":
        return [["(he/it)"], ["(s)", "(him/itself)", "(of her/it)"]]
      case "Imperf3msTo3mp":
        return [["(he/it)"], ["(s)", "(him/itself)", "(of them)"]]
      case "Imperf3msTo1cp":
        return [["(he/it)"], ["(s)", "(him/itself)", "(of us)"]]
      case "Imperf3fs":
        return [["(she/it)"], ["(s)", "(her/itself)"]]
      case "Imperf3fsTo3ms":
        return [["(she/it)"], ["(s)", "(her/itself)", "(of him/it)"]]
      case "Imperf3fsTo3fs":
        return [["(she/it)"], ["(s)", "(her/itself)", "(of her/it)"]]
      case "Imperf3fsTo1cs":
        return [["(she/it)"], ["(s)", "(her/itself)", "(of me)"]]
      case "Imperf2ms":
        return [["(you)"], ["(yourself)"]]
      case "Imperf2fs":
      case "Imperf2fsN":
        return [["(you)"], ["(yourself)"]]
      case "Imperf1cs":
        return [["(I)"], ["(myself)"]]
      case "Imperf1csTo2cs":
        return [["(I)"], ["(myself)", "(of you)"]]
      case "Imperf1csTo3fs":
        return [["(I)"], ["(myself)", "(of her/it)"]]
      case "Imperf1csTo3mp":
        return [["(I)"], ["(myself)", "(of them)"]]
      case "Imperf3mp":
      case "Imperf3mpN":
        return [["(they)"], ["(themselves)"]]
      case "Imperf3mpTo1cs":
        return [["(they)"], ["(themselves)", "(of me)"]]
      case "Imperf3mpTo2cs":
        return [["(they)"], ["(themselves)", "(of you)"]]
      case "Imperf3mpTo3ms":
      case "Imperf3mpTo3mso":
        return [["(they)"], ["(themselves)", "(of him/it)"]]
      case "Imperf3mpTo3fs":
        return [["(they)"], ["(themselves)", "(of her/it)"]]
      case "Imperf3mpTo3mp":
        return [["(they)"], ["(themselves)", "(of them)"]]
      case "Imperf3fp":
      case "Imperf3fpo":
        return [["(they)"], ["(themselves)"]]
      case "Imperf2mp":
      case "Imperf2mpN":
        return [["(you all)"], ["(yourselves)"]]
      case "Imperf2fp":
      case "Imperf2fpo":
        return [["(you all)"], ["(yourselves)"]]
      case "Imperf1cp":
        return [["(we)"], ["(ourselves)"]]
      case "Perf3ms":
        return [["(he/it has)"], ["(d)", "(him/itself)"]]
      case "Perf3msTo1cs":
        return [["(he/it has)"], ["(d)", "(him/itself)", "(of me)"]]
      case "Perf2cs":
      case "Perf2cso":
        return [["(you have)"], ["(d)", "(yourself)"]]
      case "Perf2csTo1cs":
        return [["(you have)"], ["(d)", "(yourself)", "(of me)"]]
      case "Perf2csTo3ms":
        return [["(you have)"], ["(d)", "(yourself)", "(of him/it)"]]
      case "Perf3fs":
        return [["(she/it has)"], ["(d)", "(her/itself)"]]
      case "Perf3fsTo3ms":
      case "Perf3fsTo3mso":
        return [["(she/it has)"], ["(d)", "(her/itself)", "(of him/it)"]]
      case "Perf3fsTo3fs":
        return [["(she/it has)"], ["(d)", "(her/itself)", "(of her/it)"]]
      case "Perf1cs":
        return [["(I have)"], ["(d)", "(myself)"]]
      case "Perf1csTo2cs":
        return [["(I have)"], ["(d)", "(myself)", "(of you)"]]
      case "Perf1csTo3ms":
        return [["(I have)"], ["(d)", "(myself)", "(of him/it)"]]
      case "Perf1csTo3fs":
        return [["(I have)"], ["(d)", "(myself)", "(of her/it)"]]
      case "Perf1csTo3mp":
        return [["(I have)"], ["(d)", "(myself)", "(of them)"]]
      case "Perf3cp":
        return [["(they have)"], ["(d)", "(themselves)"]]
      case "Perf3cpTo1cs":
        return [["(they have)"], ["(d)", "(themselves)", "(of me)"]]
      case "Perf3cpTo2ms":
        return [["(they have)"], ["(d)", "(themselves)", "(of you)"]]
      case "Perf2mp":
        return [["(you all have)"], ["(d)", "(yourselves)"]]
      case "Perf2fp":
        return [["(you all have)"], ["(d)", "(yourselves)"]]
      case "Perf1cp":
        return [["(we have)"], ["(d)", "(ourselves)"]]
      case "Perf1cpo":
        return [["(we have)"], ["(d)", "(ourselves)"]]
      case "Perf1cpWeak3E":
        return [["(we have)"], ["(d)", "(ourselves)"]]
      case "ImperativeTo1cs":
        return [[], ["(yourself!)", "(of me)"]]
      case "ImperativeTo1cp":
        return [[], ["(yourself!)", "(of us)"]]
      case "Imperative2ms":
        return [[], ["(yourself!)"]]
      case "Imperative2fs":
        return [[], ["(yourself!)"]]
      case "Imperative2fso":
        return [[], ["(yourself!)"]]
      case "Imperative2mp":
        return [[], ["(yourselves!)"]]
      case "Imperative2mpo":
        return [[], ["(yourselves!)"]]
      case "Imperative2fp":
        return [[], ["(yourselves!)"]]
      case "Cohort1cs":
        return [["(let me)"], ["(myself)"]]
      case "Cohort1cp":
        return [["(let us)"], ["(ourselves)"]]
    }
  } else if (passive) {
    switch (lastVerb?.id) {
      case "InfAbs":
        return [["(to be)"], ["(d)"]]
      case "InfCause":
        return [["(to be caused to be)"], ["(d)"]]
      case "ParticipleReflex1ms":
        return [["(being self)"], ["(d)"]]
      case "ParticipleReflex1fs":
        return [["(being self)"], ["(d)"]]
      case "ParticipleReflex1mp":
        return [["(being selves)"], ["(d)"]]
      case "ParticipleReflex1fp":
        return [["(being selves)"], ["(d)"]]
      case "ParticipleReflex3cs":
        return [["(being him/her/itself)"], ["(d)"]]
      case "ParticipleCause1ms":
        return [["(causing to be)"], ["(d)"]]
      case "ParticipleCause1fs":
        return [["(causing to be)"], ["(d)"]]
      case "ParticipleCause1mp":
        return [["(causing to be)"], ["(d)"]]
      case "ParticipleCause1fp":
        return [["(causing to be)"], ["(d)"]]
      case "Imperf3ms":
        return [["(he/it is)"], ["(d)"]]
      case "Imperf3msTo3fs":
        return [["(he/it is)"], ["(d)", "(of her/it)"]]
      case "Imperf3msTo3mp":
        return [["(he/it is)"], ["(d)", "(of them)"]]
      case "Imperf3msTo1cp":
        return [["(he/it is)"], ["(d)", "(of us)"]]
      case "Imperf3fs":
        return [["(she/it is)"], ["(d)"]]
      case "Imperf3fsTo3ms":
        return [["(she/it is)"], ["(d)", "(of him/it)"]]
      case "Imperf3fsTo3fs":
        return [["(she/it is)"], ["(d)", "(of her/it)"]]
      case "Imperf3fsTo1cs":
        return [["(she/it is)"], ["(d)", "(of me)"]]
      case "Imperf2ms":
        return [["(you are)"], ["(d)"]]
      case "Imperf2fs":
      case "Imperf2fsN":
        return [["(you are)"], ["(d)"]]
      case "Imperf1cs":
        return [["(I am)"], ["(d)"]]
      case "Imperf1csTo2cs":
        return [["(I am)"], ["(d)", "(of you)"]]
      case "Imperf1csTo3fs":
        return [["(I am)"], ["(d)", "(of her/it)"]]
      case "Imperf1csTo3mp":
        return [["(I am)"], ["(d)", "(of them)"]]
      case "Imperf3mp":
      case "Imperf3mpN":
        return [["(they are)"], ["(d)"]]
      case "Imperf3mpTo1cs":
        return [["(they are)"], ["(d)", "(of me)"]]
      case "Imperf3mpTo2cs":
        return [["(they are)"], ["(d)", "(of you)"]]
      case "Imperf3mpTo3ms":
      case "Imperf3mpTo3mso":
        return [["(they are)"], ["(d)", "(of him/it)"]]
      case "Imperf3mpTo3fs":
        return [["(they are)"], ["(d)", "(of her/it)"]]
      case "Imperf3mpTo3mp":
        return [["(they are)"], ["(d)", "(of them)"]]
      case "Imperf3fp":
      case "Imperf3fpo":
        return [["(they are)"], ["(d)"]]
      case "Imperf2mp":
      case "Imperf2mpN":
        return [["(you all are)"], ["(d)"]]
      case "Imperf2fp":
      case "Imperf2fpo":
        return [["(you all are)"], ["(d)"]]
      case "Imperf1cp":
        return [["(we are)"], ["(d)"]]
      case "Perf3ms":
        return [["(he/it has been)"], ["(d)"]]
      case "Perf3msTo1cs":
        return [["(he/it has been)"], ["(d)", "(of me)"]]
      case "Perf2cs":
      case "Perf2cso":
        return [["(you have been)"], ["(d)"]]
      case "Perf2csTo1cs":
        return [["(you have been)"], ["(d)", "(of me)"]]
      case "Perf2csTo3ms":
        return [["(you have been)"], ["(d)", "(of him/it)"]]
      case "Perf3fs":
        return [["(she/it has been)"], ["(d)"]]
      case "Perf3fsTo3ms":
      case "Perf3fsTo3mso":
        return [["(she/it has been)"], ["(d)", "(of him/it)"]]
      case "Perf3fsTo3fs":
        return [["(she/it has been)"], ["(d)", "(of her/it)"]]
      case "Perf1cs":
        return [["(I have been)"], ["(d)"]]
      case "Perf1csTo2cs":
        return [["(I have been)"], ["(d)", "(of you)"]]
      case "Perf1csTo3ms":
        return [["(I have been)"], ["(d)", "(of him/it)"]]
      case "Perf1csTo3fs":
        return [["(I have been)"], ["(d)", "(of her/it)"]]
      case "Perf1csTo3mp":
        return [["(I have been)"], ["(d)", "(of them)"]]
      case "Perf3cp":
        return [["(they have been)"], ["(d)"]]
      case "Perf3cpTo1cs":
        return [["(they have been)"], ["(d)", "(of me)"]]
      case "Perf3cpTo2ms":
        return [["(they have been)"], ["(d)", "(of you)"]]
      case "Perf2mp":
        return [["(you all have been)"], ["(d)"]]
      case "Perf2fp":
        return [["(you all have been)"], ["(d)"]]
      case "Perf1cp":
        return [["(we have been)"], ["(d)"]]
      case "Perf1cpo":
        return [["(we have been)"], ["(d)"]]
      case "Perf1cpWeak3E":
        return [["(we have been)"], ["(d)"]]
      case "ImperativeTo1cs":
        return [["(be)"], ["(d)", "(, you!)", "(of me)"]]
      case "ImperativeTo1cp":
        return [["(be)"], ["(d)", "(, you!)", "(of us)"]]
      case "Imperative2ms":
        return [["(be)"], ["(d)", "(, you!)"]]
      case "Imperative2fs":
        return [["(be)"], ["(d)", "(, you!)"]]
      case "Imperative2fso":
        return [["(be)"], ["(d)", "(, you!)"]]
      case "Imperative2mp":
        return [["(be)"], ["(d)", "(, you all!)"]]
      case "Imperative2mpo":
        return [["(be)"], ["(d)", "(, you all!)"]]
      case "Imperative2fp":
        return [["(be)"], ["(d)", "(, you all!)"]]
      case "Cohort1cs":
        return [["(let me be)"], ["(d)"]]
      case "Cohort1cp":
        return [["(let us be)"], ["(d)"]]
    }
  } else {
    switch (lastVerb?.id) {
      case "InfAbs":
        return [["(to)"], []]
      case "InfCause":
        return [["(to cause to)"], []]
      case "Participle3ms":
        return [["(his/its)"], ["(ing)"]]
      case "Participle3fs":
        return [["(her/its)"], ["(ing)"]]
      case "Participlemp":
        return [["(their/your/our)"], ["(ing)"]]
      case "Participlefp":
        return [["(their/your/our)"], ["(ing)"]]
      case "ParticiplePassivemp":
        return [["(their/your/our) (being)"], ["(d)"]]
      case "ParticiplePassivefp":
        return [["(their/your/our) (being)"], ["(d)"]]
      case "ParticipleReflex1ms":
        return [[], ["(ing self)"]]
      case "ParticipleReflex1fs":
        return [[], ["(ing self)"]]
      case "ParticipleReflex1mp":
        return [[], ["(ing selves)"]]
      case "ParticipleReflex1fp":
        return [[], ["(ing selves)"]]
      case "ParticipleReflex3cs":
        return [[], ["(ing him/her/itself)"]]
      case "ParticipleCause1ms":
        return [["(causing to)"], []]
      case "ParticipleCause1fs":
        return [["(causing to)"], []]
      case "ParticipleCause1mp":
        return [["(causing to)"], []]
      case "ParticipleCause1fp":
        return [["(causing to)"], []]
      case "Imperf3ms":
        return [["(he/it)"], ["(s)"]]
      case "Imperf3msTo3fs":
        return [["(he/it)"], ["(s)", "(her/it)"]]
      case "Imperf3msTo3mp":
        return [["(he/it)"], ["(s)", "(them)"]]
      case "Imperf3msTo1cp":
        return [["(he/it)"], ["(s)", "(us)"]]
      case "Imperf3fs":
        return [["(she/it)"], ["(s)"]]
      case "Imperf3fsTo3ms":
        return [["(she/it)"], ["(s)", "(him/it)"]]
      case "Imperf3fsTo3fs":
        return [["(she/it)"], ["(s)", "(her/it)"]]
      case "Imperf3fsTo3fs":
        return [["(she/it)"], ["(s)", "(me)"]]
      case "Imperf2ms":
        return [["(you)"], []]
      case "Imperf2fs":
      case "Imperf2fsN":
        return [["(you)"], []]
      case "Imperf1cs":
        return [["(I)"], []]
      case "Imperf1csTo2cs":
        return [["(I)"], ["(you)"]]
      case "Imperf1csTo3fs":
        return [["(I)"], ["(her/it)"]]
      case "Imperf1csTo3mp":
        return [["(I)"], ["(them)"]]
      case "Imperf3mp":
      case "Imperf3mpN":
        return [["(they)"], []]
      case "Imperf3mpTo1cs":
        return [["(they)"], ["(me)"]]
      case "Imperf3mpTo2cs":
        return [["(they)"], ["(you)"]]
      case "Imperf3mpTo3ms":
      case "Imperf3mpTo3mso":
        return [["(they)"], ["(him/it)"]]
      case "Imperf3mpTo3fs":
        return [["(they)"], ["(her/it)"]]
      case "Imperf3mpTo3mp":
        return [["(they)"], ["(them)"]]
      case "Imperf3fp":
      case "Imperf3fpo":
        return [["(they)"], []]
      case "Imperf2mp":
      case "Imperf2mpN":
        return [["(you all)"], []]
      case "Imperf2fp":
      case "Imperf2fpo":
        return [["(you all)"], []]
      case "Imperf1cp":
        return [["(we)"], []]
      case "Perf3ms":
        return [["(he/it has)"], ["(d)"]]
      case "Perf3msTo1cs":
        return [["(he/it has)"], ["(d)", "(me)"]]
      case "Perf2cs":
      case "Perf2cso":
        return [["(you have)"], ["(d)"]]
      case "Perf2csTo1cs":
        return [["(you have)"], ["(d)", "(me)"]]
      case "Perf2csTo3ms":
        return [["(you have)"], ["(d)", "(him/it)"]]
      case "Perf3fs":
        return [["(she/it has)"], ["(d)"]]
      case "Perf3fsTo3ms":
      case "Perf3fsTo3mso":
        return [["(she/it has)"], ["(d)", "(him/it)"]]
      case "Perf3fsTo3fs":
        return [["(she/it has)"], ["(d)", "(her/it)"]]
      case "Perf1cs":
        return [["(I have)"], ["(d)"]]
      case "Perf1csTo2cs":
        return [["(I have)"], ["(d)", "(you)"]]
      case "Perf1csTo3ms":
        return [["(I have)"], ["(d)", "(him/it)"]]
      case "Perf1csTo3fs":
        return [["(I have)"], ["(d)", "(her/it)"]]
      case "Perf1csTo3mp":
        return [["(I have)"], ["(d)", "(them)"]]
      case "Perf3cp":
        return [["(they have)"], ["(d)"]]
      case "Perf3cpTo1cs":
        return [["(they have)"], ["(d)", "(me)"]]
      case "Perf3cpTo2ms":
        return [["(they have)"], ["(d)", "(you)"]]
      case "Perf2mp":
        return [["(you all have)"], ["(d)"]]
      case "Perf2fp":
        return [["(you all have)"], ["(d)"]]
      case "Perf1cp":
        return [["(we have)"], ["(d)"]]
      case "Perf1cpo":
        return [["(we have)"], ["(d)"]]
      case "Perf1cpWeak3E":
        return [["(we have)"], ["(d)"]]
      case "ImperativeTo1cs":
        return [[], ["(me)", "(, you!)"]]
      case "ImperativeTo1cp":
        return [[], ["(us)", "(, you!)"]]
      case "Imperative2ms":
        return [[], ["(, you!)"]]
      case "Imperative2fs":
        return [[], ["(, you!)"]]
      case "Imperative2fso":
        return [[], ["(, you!)"]]
      case "Imperative2mp":
        return [[], ["(, you all!)"]]
      case "Imperative2mpo":
        return [[], ["(, you all!)"]]
      case "Imperative2fp":
        return [[], ["(, you all!)"]]
      case "Cohort1cs":
        return [["(let me)"], []]
      case "Cohort1cp":
        return [["(let us)"], []]
    }
  }
  return [[], []]
}
