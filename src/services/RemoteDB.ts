import { useState, useEffect } from "react"
import { createContainer } from "unstated-next"
import PouchDB from "pouchdb"

type RemoteInfo = {
  local?: PouchDB.Database
  host: string
  ready: boolean
}

const initialState: RemoteInfo = {
  host: "",
  ready: false,
}

export const RemoteDBState = createContainer(() => {
  const [remoteInfo, setRemoteInfo] = useState<RemoteInfo>(initialState)

  const remoteSync = (host: string, local: PouchDB.Database) => {
    setRemoteInfo({ host, local, ready: false })
  }

  const setReady = (ready: boolean) => {
    if (remoteInfo.ready !== ready) setRemoteInfo({ ...remoteInfo, ready })
  }

  const { local, host } = remoteInfo
  useEffect(() => {
    if (local && host.length > 0) {
      const remote = new PouchDB(`http://${host}:5984/${local?.name}`)
      const handle = local.sync(remote, { live: true, retry: true })
      const h: any = handle
      handle
        .on("error", (err) => {
          console.error("pouchdb error:", err)
          if (!h.canceled) setReady(false)
        })
        .on("denied", (err) => {
          console.error("pouchdb denied:", err)
          if (!h.canceled) setReady(false)
        })
        .on("change", (change) => {
          console.info("pouchdb change:", change)
          if (!h.canceled) setReady(true)
        })
        .on("complete", (info) => {
          console.info("pouchdb stopped:", info)
          if (!h.canceled) setReady(false)
        })
        .on("paused", (info) => {
          remote
            .info()
            .then(() => {
              if (!h.canceled) setReady(true)
            })
            .catch((err) => {
              if (!h.canceled) setReady(false)
              console.error("pouchdb info error:", err)
            })
        })

      return () => {
        handle.cancel()
      }
    }
  }, [local, host])

  return { remoteInfo, remoteSync }
})
