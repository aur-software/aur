import React, { useEffect } from "react"
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles"
import TopBar from "../components/TopBar"
import Verse from "../components/Verse"
import BottomBar from "../components/BottomBar"
import SettingsDrawer from "../components/SettingsDrawer"
import OccurrencesDrawer from "../components/OccurrencesDrawer"
import {
  decomposeVerseId,
  buildVerseId,
  useChapter,
} from "../services/SourceText"
import { LinearProgress, Box } from "@material-ui/core"
import { LocationState } from "../services/Location"
import { appMaxWidth, occurrencesDrawerWidth } from "../services/Geometry"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
    },
    content: {
      flexGrow: 1,
      margin: theme.spacing(1),
    },
    settingsDrawer: {},
    occurrencesDrawer: {
      [theme.breakpoints.up("sm")]: {
        width: occurrencesDrawerWidth,
        flexShrink: 0,
      },
    },
    occurrencesShift: {
      [theme.breakpoints.up("sm")]: {
        paddingLeft: occurrencesDrawerWidth,
      },
    },
    innerContent: {
      maxWidth: appMaxWidth,
      marginLeft: "auto",
      marginRight: "auto",
    },
  }),
)

export default function Home() {
  const classes = useStyles()

  const {
    location,
    params,
    setLocation,
    setParam,
    clearParam,
  } = LocationState.useContainer()

  // We use the location as the verse id.
  const verseId = location.length === 0 ? "gen.001.001" : location
  const [chapterId, verseNum] = decomposeVerseId(verseId)
  const setVerseNum = (v: number) => setLocation(buildVerseId(chapterId, v))
  const nextVerse = () => setVerseNum(verseNum + 1)
  const prevVerse = () => setVerseNum(verseNum - 1)

  // At first, we have not yet loaded the verse list for this chapter.
  // We load the verse list asynchronously, then re-render with the data.
  const verses = useChapter("wlc", chapterId)?.verses

  // Adjust state if the verseNum is out of bounds, then get the selected verse.
  useEffect(() => {
    if (!verses) return
    if (verseNum < 1) setVerseNum(1)
    if (verseNum > verses.length) setVerseNum(verses.length)
  }, [verseNum])
  const verse = verses ? verses[verseNum - 1] : null

  const isSettingsDrawerVisible = Boolean(params.settings)
  const isOccurencesDrawerVisible = Boolean(params.occur)

  const occurrencesShift = {
    [classes.occurrencesShift]: isOccurencesDrawerVisible,
  }

  return (
    <div className={classes.root}>
      <nav className={classes.settingsDrawer} aria-label="settings">
        <SettingsDrawer
          show={isSettingsDrawerVisible}
          onClose={() => clearParam("settings")}
        />
      </nav>
      <nav className={classes.occurrencesDrawer} aria-label="occurrences">
        <OccurrencesDrawer
          show={isOccurencesDrawerVisible}
          onClose={() => {
            clearParam("occur")
            clearParam("near")
          }}
          root={typeof params.occur === "string" ? params.occur : undefined}
          near={typeof params.near === "string" ? params.near : undefined}
        />
      </nav>
      <main className={classes.content}>
        <TopBar
          extraClasses={[occurrencesShift]}
          openSettings={() => setParam("settings", true)}
          openOccurrences={(word) => setParam("occur", word)}
          verseId={verseId}
        />
        <Box className={classes.innerContent}>
          {verse ? <Verse verse={verse} /> : <LinearProgress variant="query" />}
        </Box>
        <BottomBar
          extraClasses={[occurrencesShift]}
          onBack={prevVerse}
          onNext={nextVerse}
        />
      </main>
    </div>
  )
}
