// The root concordance is loaded from a (very large) file and cached in memory.
// This function will load it on the first call, then return it from memory

import { uniq, zip } from "lodash"
import {
  getBook,
  splitWordId,
  SourceBook,
  buildChapterId,
  buildVerseId,
} from "../../services/SourceText"

// on subsequent calls.
let cachedRootConcordance:
  | Map<string, [string, { ops: string[][]; at: string[] }][]>
  | undefined
async function loadRootConcordance() {
  if (cachedRootConcordance) return cachedRootConcordance!
  console.log("Root analysis loading...")

  const rootConcordance = new Map<
    string,
    [string, { ops: string[][]; at: string[] }][]
  >()

  ;(await (await fetch("wlc/concord/roots.json")).json()).forEach(
    (pair: [string, [string, { ops: string[][]; at: string[] }][]]) => {
      rootConcordance.set(...pair)
    },
  )

  console.log("Root analysis loaded.")
  cachedRootConcordance = rootConcordance
  return rootConcordance
}

export async function searchOccurrencesByRoot(
  root: string,
): Promise<{ found: [string, { at: string[] }][] }> {
  const rootEntries = (await loadRootConcordance()).get(root)
  if (!rootEntries) return { found: [] }
  return { found: rootEntries }
}

export async function searchOccurrencesNearRoot(
  root: string,
  root2: string,
  maxVerseDelta: number,
): Promise<{ found: [string, { at: string[] }][] }> {
  const rootEntries = (await loadRootConcordance()).get(root)
  const root2Entries = (await loadRootConcordance()).get(root2)
  if (!rootEntries) return { found: [] }
  if (!root2Entries) return { found: [] }

  const found: [string, { at: string[] }][] = []

  rootEntries.forEach(([word, { at }]) => {
    const foundAt: string[] = []

    at.forEach((wordId) => {
      const [bookId, chapterNum, verseNum] = splitWordId(wordId)
      const chapterId = buildChapterId(bookId, chapterNum)

      const min = buildVerseId(chapterId, verseNum - maxVerseDelta)
      const max = buildVerseId(chapterId, verseNum + maxVerseDelta) + ".999"

      if (
        root2Entries.find(([, { at }]) =>
          Boolean(at.find((id) => id > min && id < max)),
        )
      )
        foundAt.push(wordId)
    })

    if (foundAt.length > 0) found.push([word, { at: foundAt }])
  })

  return { found }
}

export async function searchOccurrencesByRootDetails(
  root: string,
  word: string,
): Promise<{ pre: string[]; post: string[] }[]> {
  const at = (await loadRootConcordance())
    .get(root)
    ?.find(([w]) => w === word)?.[1].at
  if (!at) return []

  const bookIds = uniq(at.map((wordId) => splitWordId(wordId)[0]))
  const books = await Promise.all(
    bookIds.map((bookId) => getBook("wlc", bookId)),
  )
  const booksById: { [key: string]: SourceBook } = {}
  zip(bookIds, books).forEach(([bookId, book]) => (booksById[bookId!] = book!))

  const details = at.map((wordId) => {
    const [bookId, chapterNum, verseNum, wordNum] = splitWordId(wordId)
    const book = booksById[bookId]!
    const chapter = book.chapters[chapterNum - 1]!
    const verse = chapter.verses[verseNum - 1]!
    const pre = verse.words.slice(undefined, wordNum - 1).map((w) => w.source)
    const post = verse.words.slice(wordNum).map((w) => w.source)
    return { pre, post }
  })

  return details
}
