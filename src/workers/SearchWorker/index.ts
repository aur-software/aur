// eslint-disable-next-line import/no-webpack-loader-syntax
import SearchWorker from "comlink-loader!./worker"
import { useState } from "react"
import useAsyncEffect from "@n1ru4l/use-async-effect"

export default SearchWorker

SearchWorker.global = new SearchWorker()

export function useSearchOccurrencesByRoot(root: string | undefined) {
  const [result, setResult] = useState<
    { found: [string, { at: string[] }][] } | undefined
  >()
  useAsyncEffect(
    function* (_, c) {
      setResult(undefined)
      if (!root) return
      setResult(yield* c(SearchWorker.global.searchOccurrencesByRoot(root)))
    },
    [root],
  )
  return result
}

export function useSearchOccurrencesNearRoot(
  root: string | undefined,
  root2: string | undefined,
  maxVerseDelta: number,
) {
  const [result, setResult] = useState<
    { found: [string, { at: string[] }][] } | undefined
  >()
  useAsyncEffect(
    function* (_, c) {
      setResult(undefined)
      if (!root || !root2) return
      setResult(
        yield* c(
          SearchWorker.global.searchOccurrencesNearRoot(
            root,
            root2,
            maxVerseDelta,
          ),
        ),
      )
    },
    [root],
  )
  return result
}

export function useSearchOccurrencesByRootDetails(
  root: string | undefined,
  word: string | undefined,
) {
  const [result, setResult] = useState<
    { pre: string[]; post: string[] }[] | undefined
  >()
  useAsyncEffect(
    function* (_, c) {
      setResult(undefined)
      if (!root || !word) return
      setResult(
        yield* c(
          SearchWorker.global.searchOccurrencesByRootDetails(root, word),
        ),
      )
    },
    [root],
  )
  return result
}
