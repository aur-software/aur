declare module "comlink-loader!*" {
  class SearchWorker extends Worker {
    constructor()

    static global: SearchWorker

    // All exported functions from worker.ts should be declared here:
    searchOccurrencesByRoot(
      root: string,
    ): Promise<{ found: [string, { at: string[] }][] }>
    searchOccurrencesNearRoot(
      root: string,
      root2: string,
      maxVerseDelta: number,
    ): Promise<{ found: [string, { at: string[] }][] }>
    searchOccurrencesByRootDetails(
      root: string,
      word: string,
    ): Promise<{ pre: string[]; post: string[] }[]>
  }

  export = SearchWorker
}
