import React, { useEffect, useMemo } from "react"
import CssBaseline from "@material-ui/core/CssBaseline"
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles"
import { LocationState } from "./services/Location"
import { SettingsState } from "./services/Settings"
import Home from "./pages/Home"
import { RemoteDBState } from "./services/RemoteDB"
import { analysisDB } from "./services/Analysis"
import "./App.css"

function App() {
  return (
    <LocationState.Provider>
      <SettingsState.Provider>
        <RemoteDBState.Provider>
          <AppWithTheme />
        </RemoteDBState.Provider>
      </SettingsState.Provider>
    </LocationState.Provider>
  )
}

export default App

const lightTheme = createMuiTheme({
  palette: {
    type: "light",
    contrastThreshold: 0.5,
    primary: { main: "#4527a0" },
    secondary: { main: "#fb6400" },
    background: { paper: "#ffffff", default: "#fef7de" },
  },
})

const darkTheme = createMuiTheme({
  palette: {
    type: "dark",
    contrastThreshold: 0.5,
    primary: { main: "#4527a0" },
    secondary: { main: "#fb6400" },
    background: { paper: "#181518", default: "#201f24" },
    text: {
      primary: "rgba(254, 247, 222, 0.87)",
      secondary: "rgba(254, 247, 222, 0.54)",
      disabled: "rgba(254, 247, 222, 0.38)",
    },
    action: {
      active: "rgba(254, 247, 222, 0.54)",
      hover: "rgba(254, 247, 222, 0.04)",
      selected: "rgba(254, 247, 222, 0.08)",
      disabled: "rgba(254, 247, 222, 0.26)",
      disabledBackground: "rgba(254, 247, 222, 0.12)",
    },
    divider: "rgba(254, 247, 222, 0.12)",
  },
})

function AppWithTheme() {
  const { settings } = SettingsState.useContainer()
  const { remoteSync } = RemoteDBState.useContainer()

  useEffect(() => {
    remoteSync(settings.remoteDBHost, analysisDB.pouchdb)
  }, [settings.remoteDBHost])

  return (
    <ThemeProvider theme={settings.darkTheme ? darkTheme : lightTheme}>
      <CssBaseline />
      {useMemo(() => {
        if (!settings.notReady) return <Home />
      }, [settings.notReady])}
    </ThemeProvider>
  )
}
