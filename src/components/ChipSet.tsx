import React from "react"
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles"
import { Box } from "@material-ui/core"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    chipset: {
      display: "flex",
      justifyContent: "center",
      flexWrap: "wrap",
      flexGrow: 1,
      textAlign: "center",
      height: "min-content",
      alignSelf: "center",
    },
  }),
)

interface ShowBookChipsProps {
  children: React.ReactNode[]
}

export default function ShowBookChips(props: ShowBookChipsProps) {
  const classes = useStyles()
  const { children } = props

  return <Box className={classes.chipset}>{children}</Box>
}
