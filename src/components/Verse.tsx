import React from "react"
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles"
import { Box, TextField } from "@material-ui/core"
import Word from "./Word"
import { SourceVerse } from "../services/SourceText"
import { VerseAnalysisState, WordAnalysisState } from "../services/Analysis"
import { SettingsState } from "../services/Settings"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    translationText: {
      width: "100%",
      paddingTop: theme.spacing(2),
    },
  }),
)

export interface VerseProps {
  verse: SourceVerse
}

export default function Verse(props: VerseProps) {
  const { verse } = props
  return (
    <VerseAnalysisState.Provider key={verse.id} initialState={verse}>
      <VerseInner verse={verse} />
    </VerseAnalysisState.Provider>
  )
}

function VerseInner(props: VerseProps) {
  const classes = useStyles()
  const { verse } = props
  const { settings } = SettingsState.useContainer()
  const {
    translation,
    saveVerseTranslation,
  } = VerseAnalysisState.useContainer()

  return (
    <Box>
      {verse.words.map((word) => (
        <WordAnalysisState.Provider key={word.id} initialState={word}>
          <Word word={word} />
        </WordAnalysisState.Provider>
      ))}
      {settings.user.length > 0 && (
        <TextField
          className={classes.translationText}
          multiline
          defaultValue={translation?.text ?? ""}
          onKeyDown={(e) => {
            if (e.key === "Enter") {
              saveVerseTranslation((e.target as any).value)
              e.preventDefault()
              e.stopPropagation()
              ;(e.target as any).blur()
            }
          }}
          onBlur={(e) => {
            saveVerseTranslation((e.target as any).value)
          }}
        />
      )}
    </Box>
  )
}
