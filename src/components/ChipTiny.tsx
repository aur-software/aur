import React from "react"
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles"
import { Chip } from "@material-ui/core"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    tiny: {
      margin: 0,
      fontSize: "0.7em",
      height: "1.5em",
      backgroundColor: "rgba(0, 0, 0, 0)",
      "& > *": {
        paddingLeft: theme.spacing(0.5),
        paddingRight: theme.spacing(0.5),
      },
    },
  }),
)

interface ChipTinyProps {
  label: string
}

export default function ChipTiny(props: ChipTinyProps) {
  const classes = useStyles()
  const { label } = props

  return (
    <Chip className={classes.tiny} key={label} label={label} size="small" />
  )
}
