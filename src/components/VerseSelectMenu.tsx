import React, { useState, useMemo } from "react"
import useAsyncEffect from "@n1ru4l/use-async-effect"
import { Menu, MenuItem, CircularProgress } from "@material-ui/core"
import { LocationState } from "../services/Location"
import { listBooks, listChapters, getChapter } from "../services/SourceText"

// The VerseSelectMenu is spawned when its anchorEl prop is an HTMLElement.
// The user is prompted to select a book in the first menu, then a chapter in
// the next menu, and then finally the verse in the chapter.
// Finally selecting a verse in the last menu has effect on LocationState.
// When the menu is closed for any reason (success or abort), onClose is called.

export interface VerseSelectMenuProps {
  anchorEl: HTMLElement | null
  onClose: () => any
}

export default function VerseSelectMenu(props: VerseSelectMenuProps) {
  const { anchorEl, onClose } = props
  const { setLocation } = LocationState.useContainer()

  // This state tracks the successive menu selections that the user has made.
  // After the user selects a book, we track it as nextBookSelected.
  // After the user selects a chapter, we track it as nextChapterSelected.
  // The nextChapterSelected triggers the async load of versesInNextChapter.
  // The user's final verse selection will be communicated back to nonlocal
  // state via the setLocation call, instead of any state shown here.
  const [nextBookSelected, setNextBookSelected] = useState<string | undefined>()
  const [nextChapterSelected, setNextChapterSelected] = useState<
    string | undefined
  >()
  const [versesInNextChapter, setVersesInNextChapter] = useState<
    { name: string; verseId: string }[] | undefined
  >()

  // Show the various menus based on the state of what has been selected so far.
  // Don't show anything if the anchorEl prop is undefined/disabled.
  const showBookMenu =
    Boolean(anchorEl) &&
    !Boolean(nextBookSelected) &&
    !Boolean(nextChapterSelected)
  const showChapterMenu =
    Boolean(anchorEl) &&
    Boolean(nextBookSelected) &&
    !Boolean(nextChapterSelected)
  const showVerseMenu =
    Boolean(anchorEl) &&
    Boolean(nextBookSelected) &&
    Boolean(nextChapterSelected)

  // When we close for any reason, clean up and reset any/all state set so far,
  // then call the onClose callback function that was provided in the props.
  const close = () => {
    setVersesInNextChapter(undefined)
    setNextChapterSelected(undefined)
    setNextBookSelected(undefined)
    onClose() // we assume this will clear anchorEl, but it's up to the parent.
  }

  // Show listBooks as the bookMenuItems.
  const bookMenuItems = useMemo(
    () =>
      listBooks().map(({ bookId, name }) => (
        <MenuItem key={bookId} onClick={() => setNextBookSelected(bookId)}>
          {name}
        </MenuItem>
      )),
    [],
  )

  // Show listChapters for nextBookSelected as the chapterMenuItems.
  const chapterMenuItems = useMemo(
    () =>
      nextBookSelected
        ? listChapters(nextBookSelected).map(({ chapterId, number }) => (
            <MenuItem
              key={chapterId}
              onClick={() => setNextChapterSelected(chapterId)}
            >
              {number}
            </MenuItem>
          ))
        : null,
    [nextBookSelected],
  )

  // When nextChapterSelected changes, asynchronously fetch versesInNextChapter.
  useAsyncEffect(
    function* (_, c) {
      if (!nextChapterSelected) return
      const chapter = yield* c(getChapter("wlc", nextChapterSelected))
      setVersesInNextChapter(
        chapter.verses.map((verse) => ({
          verseId: verse.id,
          name: verse.name,
        })),
      )
    },
    [nextChapterSelected],
  )

  // Show versesInNextChapter as the verseMenuItems.
  // If they haven't loaded yet, show a progress circle.
  const verseMenuItems = useMemo(
    () =>
      versesInNextChapter ? (
        versesInNextChapter.map(({ verseId, name }) => (
          <MenuItem
            key={verseId}
            onClick={(e) => {
              close()
              setLocation(verseId)
            }}
          >
            {name}
          </MenuItem>
        ))
      ) : (
        <MenuItem disabled>
          <CircularProgress />
        </MenuItem>
      ),
    [versesInNextChapter],
  )

  return (
    <React.Fragment>
      <Menu anchorEl={anchorEl} open={showBookMenu} onClose={() => close()}>
        {bookMenuItems}
      </Menu>
      <Menu anchorEl={anchorEl} open={showChapterMenu} onClose={() => close()}>
        {chapterMenuItems}
      </Menu>
      <Menu anchorEl={anchorEl} open={showVerseMenu} onClose={() => close()}>
        {verseMenuItems}
      </Menu>
    </React.Fragment>
  )
}
