import React from "react"
import {
  createStyles,
  Theme,
  useTheme,
  makeStyles,
} from "@material-ui/core/styles"
import {
  Drawer,
  FormControl,
  FormHelperText,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@material-ui/core"
import RefreshIcon from "@material-ui/icons/Refresh"
import { SettingsState } from "../services/Settings"
import {
  allScriptNamesAndLabels,
  buildWordDisplayStyle,
  buildWordDisplayText,
  ScriptName,
} from "../services/Scripts"
import { RemoteDBState } from "../services/RemoteDB"

const drawerWidth = 200

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    drawerPaper: {
      width: drawerWidth,
      padding: theme.spacing(2),
    },
    title: {
      paddingBottom: theme.spacing(2),
    },
    formControl: {
      width: "100%",
      marginBottom: theme.spacing(2),
    },
    formControlAsRow: {
      width: "100%",
      marginBottom: theme.spacing(2),
      flexDirection: "row",
    },
  }),
)

export interface SettingsDrawerProps {
  show: boolean
  onClose: () => any
}

export default function SettingsDrawer(props: SettingsDrawerProps) {
  const classes = useStyles()
  const theme = useTheme()
  const { show, onClose } = props
  const {
    settings,
    setDisplayScript,
    setDarkTheme,
    setUser,
    setRemoteDBHost,
  } = SettingsState.useContainer()
  const { remoteInfo } = RemoteDBState.useContainer()

  const drawer = (
    <React.Fragment>
      <Typography noWrap variant="h5" className={classes.title}>
        Settings
      </Typography>

      <FormControl className={classes.formControl}>
        <InputLabel>Display Script</InputLabel>
        <Select
          value={settings.displayScript}
          onChange={(e) => setDisplayScript(e.target.value as ScriptName)}
        >
          {allScriptNamesAndLabels.map(({ name, label }) => (
            <MenuItem key={name} value={name}>
              {label}
            </MenuItem>
          ))}
        </Select>
        <FormHelperText style={buildWordDisplayStyle(22, settings)}>
          {buildWordDisplayText("בְּרֵאשִׁ֖ית", settings)}
        </FormHelperText>
      </FormControl>

      <FormControl className={classes.formControl}>
        <InputLabel>Color Theme</InputLabel>
        <Select
          value={settings.darkTheme ? "dark" : "light"}
          onChange={(e) => setDarkTheme(e.target.value === "dark")}
        >
          <MenuItem value="light">White Background</MenuItem>
          <MenuItem value="dark">Black Background</MenuItem>
        </Select>
      </FormControl>

      <FormControl className={classes.formControlAsRow}>
        <TextField
          id="user"
          label="Username"
          helperText="(Interpretation Save/Recall)"
          InputLabelProps={{ shrink: true }}
          onKeyUp={(e) => {
            if (e.key === "Enter") setUser((e.target as any).value)
          }}
        />
        <IconButton
          edge="end"
          onClick={() => {
            setUser((document.getElementById("user") as any).value)
          }}
        >
          <RefreshIcon fontSize="small" />
        </IconButton>
      </FormControl>

      <FormControl className={classes.formControlAsRow}>
        <TextField
          id="remote-db-host"
          label="Remote Database"
          InputLabelProps={{ shrink: true }}
          error={!remoteInfo.ready && remoteInfo.host.length > 0}
          placeholder={remoteInfo.host}
          helperText={
            remoteInfo.host.length === 0
              ? "Not Configured"
              : remoteInfo.ready
              ? "Connected"
              : "Not Connected"
          }
          onKeyUp={(e) => {
            if (e.key === "Enter") setRemoteDBHost((e.target as any).value)
          }}
        />
        <IconButton
          edge="end"
          onClick={() => {
            setRemoteDBHost(
              (document.getElementById("remote-db-host") as any).value,
            )
          }}
        >
          <RefreshIcon fontSize="small" />
        </IconButton>
      </FormControl>
    </React.Fragment>
  )

  return (
    <Drawer
      variant="temporary"
      anchor={theme.direction === "rtl" ? "right" : "left"}
      open={show}
      onClose={onClose}
      classes={{
        paper: classes.drawerPaper,
      }}
      ModalProps={{
        keepMounted: true, // Better open performance on mobile.
      }}
    >
      {drawer}
    </Drawer>
  )
}
