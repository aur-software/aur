import React, { useState } from "react"
import { makeStyles } from "@material-ui/core/styles"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import DialogTitle from "@material-ui/core/DialogTitle"
import Dialog from "@material-ui/core/Dialog"
import {
  TextField,
  Typography,
  FormControlLabel,
  Switch,
  DialogContent,
  DialogActions,
  Button,
} from "@material-ui/core"
import { SettingsState } from "../services/Settings"
import {
  WordSummary,
  saveWordSummary,
  deleteWordSummary,
} from "../services/Analysis"
import { untransliterateToHebrewWord } from "../services/SourceText"
import {
  buildWordDisplayStyle,
  buildWordDisplayText,
} from "../services/Scripts"

const useStyles = makeStyles({
  title: {
    textAlign: "center",
  },
})

export interface RootEditDialogProps {
  summary: WordSummary
  open: boolean
  onClose: () => any
}

export default function RootEditDialog(props: RootEditDialogProps) {
  const classes = useStyles()
  const { summary, open, onClose } = props
  const { settings } = SettingsState.useContainer()

  const [newSummary, setNewSummary] = useState<WordSummary>(summary)

  const hasChanges =
    newSummary.prefix !== summary.prefix ||
    (newSummary.main !== summary.main && !newSummary.proper) ||
    newSummary.suffix !== summary.suffix ||
    newSummary.dubious !== summary.dubious ||
    newSummary.particle !== summary.particle ||
    newSummary.proper !== summary.proper ||
    newSummary.notes !== summary.notes

  const handleDelete = () => {
    deleteWordSummary(summary.word)
    onClose()
  }
  const handleSave = () => {
    saveWordSummary(summary.word, newSummary)
    onClose()
  }

  return (
    <Dialog onClose={onClose} aria-labelledby="root-definition" open={open}>
      <DialogTitle className={classes.title} id="root-definition">
        Root Definition
      </DialogTitle>
      <DialogContent>
        <Typography
          className={classes.title}
          style={buildWordDisplayStyle(45, settings)}
        >
          {buildWordDisplayText(
            untransliterateToHebrewWord(summary.word),
            settings,
          )}
        </Typography>
        <List>
          <ListItem>
            <TextField
              id="root-summary-pre"
              label="Summary Prefix"
              InputLabelProps={{ shrink: true }}
              value={newSummary.prefix}
              size="small"
              onChange={(e) =>
                setNewSummary({ ...newSummary, prefix: e.target.value })
              }
            />
          </ListItem>
          <ListItem>
            <TextField
              id="root-summary-main"
              label="Summary"
              InputLabelProps={{ shrink: true }}
              size="small"
              value={newSummary.proper ? summary.word : newSummary.main}
              disabled={newSummary.proper}
              onChange={(e) =>
                setNewSummary({ ...newSummary, main: e.target.value })
              }
            />
          </ListItem>
          <ListItem>
            <TextField
              id="root-summary-post"
              label="Summary Suffix"
              InputLabelProps={{ shrink: true }}
              size="small"
              value={newSummary.suffix}
              onChange={(e) =>
                setNewSummary({ ...newSummary, suffix: e.target.value })
              }
            />
          </ListItem>
          <ListItem>
            <FormControlLabel
              control={<Switch name="root-dubious" />}
              label="Dubious"
              checked={newSummary.dubious}
              onChange={() =>
                setNewSummary({ ...newSummary, dubious: !newSummary.dubious })
              }
            />
          </ListItem>
          <ListItem>
            <FormControlLabel
              control={<Switch name="root-proper" />}
              label="Proper Name"
              checked={newSummary.proper}
              onChange={() =>
                setNewSummary({ ...newSummary, proper: !newSummary.proper })
              }
            />
          </ListItem>
          <ListItem>
            <FormControlLabel
              control={<Switch name="root-particle" />}
              label="Particle"
              checked={newSummary.particle}
              onChange={() =>
                setNewSummary({ ...newSummary, particle: !newSummary.particle })
              }
            />
          </ListItem>
          <ListItem>
            <TextField
              id="root-summary-notes"
              label="Other Notes"
              InputLabelProps={{ shrink: true }}
              size="small"
              style={{ width: "100%" }}
              InputProps={{ style: { fontSize: "0.75em" } }}
              value={newSummary.notes}
              onChange={(e) =>
                setNewSummary({ ...newSummary, notes: e.target.value })
              }
              multiline
            />
          </ListItem>
        </List>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleDelete} color="secondary">
          Delete
        </Button>
        <Button onClick={handleSave} disabled={!hasChanges} color="primary">
          {hasChanges ? "Save Changes" : "No Changes"}
        </Button>
      </DialogActions>
    </Dialog>
  )
}
