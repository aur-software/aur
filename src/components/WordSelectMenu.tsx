import React, { useState } from "react"
import { Menu, MenuItem } from "@material-ui/core"
import { SettingsState } from "../services/Settings"
import {
  buildWordDisplayStyle,
  buildWordDisplayText,
} from "../services/Scripts"
import {
  allHebrewGlyphs,
  transliterateHebrewWord,
} from "../services/SourceText"

// The VerseSelectMenu is spawned when its anchorEl prop is an HTMLElement.
// The user is prompted to select a book in the first menu, then a chapter in
// the next menu, and then finally the verse in the chapter.
// Finally selecting a verse in the last menu has effect on LocationState.
// When the menu is closed for any reason (success or abort), onClose is called.

export interface VerseSelectMenuProps {
  anchorEl: HTMLElement | null
  onFinish: (word: string) => any
  onClose: () => any
}

export default function VerseSelectMenu(props: VerseSelectMenuProps) {
  const { anchorEl, onFinish, onClose } = props
  const { settings } = SettingsState.useContainer()

  // This state tracks the successive glyph selections that the user has made.
  const [word, setWord] = useState<string>("")
  const [lastWord, setLastWord] = useState<string>("")

  // When we close for any reason, clean up and reset any/all state set so far,
  // then call the onClose callback function that was provided in the props.
  const close = () => {
    setWord("")
    onClose() // we assume this will clear anchorEl, but it's up to the parent.
  }

  // Finishing sends the notification about the selected word and then closes.
  const finish = (useWord: string) => {
    setLastWord(useWord)
    onFinish(transliterateHebrewWord(useWord))
    close()
  }

  return (
    <React.Fragment>
      <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={close}>
        {word.length === 0 && lastWord.length > 0 ? (
          <MenuItem
            onClick={() => finish(lastWord)}
            style={{ color: "green", ...buildWordDisplayStyle(26, settings) }}
          >
            {buildWordDisplayText(lastWord, settings)} ✓
          </MenuItem>
        ) : null}
        {word.length >= 2 ? (
          <MenuItem
            onClick={() => finish(word)}
            style={{ color: "green", ...buildWordDisplayStyle(26, settings) }}
          >
            {buildWordDisplayText(word, settings)} ✓
          </MenuItem>
        ) : null}
        {allHebrewGlyphs().map((glyph) => (
          <MenuItem
            key={glyph}
            onClick={() => setWord(word + glyph)}
            style={buildWordDisplayStyle(26, settings)}
          >
            {buildWordDisplayText(word + glyph, settings)}...
          </MenuItem>
        ))}
      </Menu>
    </React.Fragment>
  )
}
