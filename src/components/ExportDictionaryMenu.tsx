import React, { useState } from "react"
import useAsyncEffect from "@n1ru4l/use-async-effect"
import { Menu, MenuItem, Link, CircularProgress } from "@material-ui/core"
import { exportDictionaryAsJSON } from "../services/Analysis"

// The ExportDictionaryMenu is spawned when its anchorEl is an HTMLElement.
// The user is prompted to download the dictionary as a JSON file.

export interface ExportDictionaryMenuProps {
  anchorEl: HTMLElement | null
  onClose: () => any
}

export default function ExportDictionaryMenu(props: ExportDictionaryMenuProps) {
  const { anchorEl, onClose } = props
  const show = Boolean(anchorEl)

  // This represents the entire contents of the dictionary as a JSON string,
  // exposed as a Blob URL that points to the contents of that string.
  // When first mounted, the Blob URL is undefined.
  // Whenever show becomes true, populate from the Blob URL from the database.
  // When loaded again, we revoke the previous Blob URL to avoid memory leak.
  const [blobUrl, setBlobUrlInner] = useState<string | undefined>()
  const setBlobUrl = (newBlobUrl: string | undefined) => {
    if (blobUrl) URL.revokeObjectURL(blobUrl)
    setBlobUrlInner(newBlobUrl)
  }
  const setBlobUrlFromJsonString = (string: string) => {
    const blob = new Blob([string], { type: "application/json" })
    setBlobUrl(URL.createObjectURL(blob))
  }

  useAsyncEffect(
    function* (_, c) {
      const json = yield* c(exportDictionaryAsJSON())
      setBlobUrlFromJsonString(json)
    },
    [show],
  )

  return (
    <Menu anchorEl={anchorEl} open={show} onClose={() => {}}>
      <MenuItem>
        {blobUrl ? (
          <Link
            color="inherit"
            href={blobUrl}
            download="aur-dictionary.json"
            onClick={onClose}
          >
            Click Here to Save the Dictionary File
          </Link>
        ) : (
          <CircularProgress />
        )}
      </MenuItem>
    </Menu>
  )
}
