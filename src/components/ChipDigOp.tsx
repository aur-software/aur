import React from "react"
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles"
import { Avatar, Chip } from "@material-ui/core"
import { untransliterateToHebrewWord } from "../services/SourceText"
import {
  buildWordDisplayStyle,
  buildWordDisplayText,
} from "../services/Scripts"
import { SettingsState } from "../services/Settings"
import { DigOp } from "../services/Dig"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    chip: {
      color: "inherit",
      margin: theme.spacing(0.25),
    },
  }),
)

interface ChipDigOpProps {
  op: DigOp
}

export default function ChipDigOp(props: ChipDigOpProps) {
  const classes = useStyles()
  const { op } = props
  const { settings } = SettingsState.useContainer()

  return op.chipName ? (
    <Chip
      key={op.id}
      className={classes.chip}
      size="small"
      label={op.chipName}
      avatar={
        op.chipGlyphs ? (
          <Avatar style={buildWordDisplayStyle(12, settings)}>
            {buildWordDisplayText(
              untransliterateToHebrewWord(op.chipGlyphs),
              settings,
            )}
          </Avatar>
        ) : undefined
      }
    />
  ) : null
}
