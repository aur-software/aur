import React, { useState } from "react"
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles"
import {
  Badge,
  Box,
  Checkbox,
  CircularProgress,
  Divider,
  Accordion,
  AccordionDetails,
  AccordionSummary,
  List,
  ListItem,
  Radio,
  Typography,
  ButtonBase,
} from "@material-ui/core"
import { green } from "@material-ui/core/colors"
import ExpandMoreIcon from "@material-ui/icons/ExpandMore"
import EditIcon from "@material-ui/icons/Edit"
import { SourceWord, untransliterateToHebrewWord } from "../services/SourceText"
import {
  buildWordDisplayStyle,
  buildWordDisplayText,
} from "../services/Scripts"
import { SettingsState } from "../services/Settings"
import { WordAnalysisState } from "../services/Analysis"
import {
  Analysis,
  WordSummary,
  applyDigToRootSummary,
  AnalysisAlternative,
  makeWordSummary,
} from "../services/Analysis"
import RootEditDialog from "./RootEditDialog"
import ChipSet from "./ChipSet"
import ChipDigOp from "./ChipDigOp"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      // Allow selecting text on the word panel.
      "& .MuiAccordionSummary-root": {
        userSelect: "auto",
      },
    },
    editRootButtonBase: {
      // Allow selecting text on the root edit button base.
      userSelect: "text",
    },
    panelDetails: {
      padding: theme.spacing(1),
    },
    altList: {
      width: "100%",
      padding: 0,
    },
    altListItem: {
      display: "flex",
      flexDirection: "row",
      width: "100%",
    },
    editIcon: {
      opacity: 0.5,
      transform: "scale(0.5)",
      margin: theme.spacing(0.5),
    },
    leftColumn: {
      width: "38.2%",
      textAlign: "center",
      height: "min-content",
      alignSelf: "center",
    },
    rightColumn: {
      width: "61.8%",
      textAlign: "center",
      height: "min-content",
      alignSelf: "center",
    },
    greenCheckbox: { "&$greenCheckboxChecked": { color: green[600] } },
    greenCheckboxChecked: {},
  }),
)

interface ShowWordSummaryProps {
  summary: WordSummary | undefined
  hideEmptyLines?: boolean
  showNotes?: boolean
}

function ShowWordSummary(props: ShowWordSummaryProps) {
  const { summary, hideEmptyLines, showNotes } = props

  const textPrimaryOrError = summary?.dubious ? "error" : "textPrimary"
  const textSecondaryOrError = summary?.dubious ? "error" : "textSecondary"
  const maybeBold: React.CSSProperties = summary?.proper
    ? { fontStyle: "italic" }
    : {}

  return (
    <React.Fragment>
      {summary?.prefix || !hideEmptyLines ? (
        <Typography
          variant="body2"
          component="p"
          style={{ wordBreak: "break-word" }}
          color={textSecondaryOrError}
        >
          &nbsp;{summary?.prefix}&nbsp;
        </Typography>
      ) : null}
      {summary?.main || !hideEmptyLines ? (
        <Typography
          variant="body2"
          component="p"
          style={{ wordBreak: "break-word", ...maybeBold }}
          color={textPrimaryOrError}
        >
          &nbsp;{summary?.main}&nbsp;
        </Typography>
      ) : null}
      {summary?.suffix || !hideEmptyLines ? (
        <Typography
          variant="body2"
          component="p"
          style={{ wordBreak: "break-word" }}
          color={textSecondaryOrError}
        >
          &nbsp;{summary?.suffix}&nbsp;
        </Typography>
      ) : null}
      {summary?.notes && showNotes ? (
        <Typography style={{ fontSize: "0.6em", opacity: "0.5" }}>
          {summary.notes}
        </Typography>
      ) : null}
    </React.Fragment>
  )
}

interface ShowAnalysisAlternativeProps {
  result: AnalysisAlternative
}

function ShowAnalysisAlternative(props: ShowAnalysisAlternativeProps) {
  const classes = useStyles()
  const { result } = props
  const { settings } = SettingsState.useContainer()

  const [rootDialogOpen, setRootDialogOpen] = useState(false)

  return (
    <Box
      className={classes.altListItem}
      color={result.rootSummary ? "text.primary" : "text.disabled"}
    >
      <RootEditDialog
        summary={result.rootSummary || makeWordSummary(result.dig.root, {})}
        open={rootDialogOpen}
        onClose={() => setRootDialogOpen(false)}
      />
      <Box className={classes.leftColumn}>
        <ShowWordSummary
          summary={result.rootSummary}
          hideEmptyLines
          showNotes
        />
        <ChipSet>
          {result.dig.ops.map((op) => (
            <ChipDigOp key={op.id} op={op} />
          ))}
        </ChipSet>
      </Box>
      <Box className={classes.rightColumn}>
        <ButtonBase
          className={classes.editRootButtonBase}
          onClick={() => setRootDialogOpen(true)}
        >
          <EditIcon className={classes.editIcon} style={{ opacity: 0 }} />
          <Typography style={buildWordDisplayStyle(30, settings)}>
            {buildWordDisplayText(
              untransliterateToHebrewWord(result.dig.root),
              settings,
            )}
          </Typography>
          <EditIcon className={classes.editIcon} />
        </ButtonBase>
      </Box>
    </Box>
  )
}

interface ShowAnalysisAlternativesProps {
  analysis: Analysis
  resultIndex: number
  resultIndexOverride: number | undefined
  setResultIndexOverride: (n: number | undefined) => any
}

function ShowAnalysisAlternatives(props: ShowAnalysisAlternativesProps) {
  const classes = useStyles()
  const {
    analysis,
    resultIndex,
    resultIndexOverride,
    setResultIndexOverride,
  } = props

  const [showUndefined, setShowUndefined] = useState(false)

  return (
    <List className={classes.altList}>
      {analysis.results.map((result, index) =>
        result.rootSummary || showUndefined ? (
          <ListItem key={index} className={classes.altListItem}>
            <ShowAnalysisAlternative result={result} />
            <Radio
              edge="end"
              checked={index === resultIndexOverride ?? resultIndex}
              onChange={() => setResultIndexOverride(index)}
            />
          </ListItem>
        ) : null,
      )}
      {analysis.results.length > analysis.knownResultCount ? (
        <ListItem
          className={classes.altListItem}
          style={{ paddingTop: 0, paddingBottom: 0 }}
        >
          <div className={classes.altListItem}>
            <Box className={classes.leftColumn}></Box>
            <Box
              className={classes.rightColumn}
              color={showUndefined ? "text.secondary" : "text.disabled"}
            >
              <Typography variant="body2">Show unknown roots</Typography>
            </Box>
          </div>
          <Checkbox
            edge="end"
            checked={showUndefined}
            onChange={() => setShowUndefined(!showUndefined)}
          />
        </ListItem>
      ) : null}
    </List>
  )
}

export interface WordProps {
  word: SourceWord
}

export default function Word(props: WordProps) {
  const classes = useStyles()
  const { word } = props
  const { settings } = SettingsState.useContainer()
  const {
    analysis,
    saveWordInterpretationIndex,
    clearWordInterpretation,
  } = WordAnalysisState.useContainer()

  const [expanded, setExpanded] = useState<boolean>(false)

  const [resultIndexOverride, setResultIndexOverride] = useState<
    number | undefined
  >(undefined)
  const resultIndex =
    resultIndexOverride ??
    analysis?.savedResultIndex ??
    analysis?.resultIndex ??
    0
  const mainResult = analysis?.results[resultIndex]
  const summary = mainResult?.rootSummary
    ? applyDigToRootSummary(mainResult.rootSummary, mainResult.dig.ops)
    : undefined

  // If we have analysis loaded, show a summary of it on the lefthand column.
  // Otherwise show a progress spinner while we continue to load it.
  const leftColumn = analysis ? (
    <ShowWordSummary summary={summary} />
  ) : (
    <CircularProgress
      style={{
        // Vary the animation speed of each spinner (per group of 10),
        // based on the last numerical digit of the word id.
        animationDuration: `${
          1 + 0.4 * (word.id.charCodeAt(word.id.length - 1) - 48)
        }s`,
      }}
    />
  )

  // When there are multiple interpretations, show a badge indicating this
  // on the expand icon, to entice the user to expand and peruse.
  // If there are multiple known root possibilities among the alternatives,
  // the color is secondary to make it extra noticeable.
  const knownResultCount = analysis?.knownResultCount || 0
  const knownRootCount = analysis?.knownRoots.size || 0
  const expandIcon =
    knownResultCount > 1 ? (
      <Badge
        badgeContent={knownResultCount}
        color={knownRootCount > 1 ? "secondary" : "primary"}
        invisible={expanded}
      >
        <ExpandMoreIcon />
      </Badge>
    ) : (
      <ExpandMoreIcon />
    )

  return (
    <Accordion
      className={classes.root}
      onChange={(e, expanded) => setExpanded(expanded)}
    >
      <AccordionSummary
        expandIcon={expandIcon}
        disabled={!analysis}
        aria-controls={`${word.id}-content`}
        id={`${word.id}-header`}
      >
        <Box className={classes.leftColumn}>{leftColumn}</Box>
        <Typography
          className={classes.rightColumn}
          style={buildWordDisplayStyle(30, settings)}
          variant="h4"
          component="h4"
        >
          {buildWordDisplayText(word.source, settings)}
        </Typography>

        {settings.user.length > 0 && (
          <Checkbox
            color="default"
            classes={{
              root: classes.greenCheckbox,
              checked: classes.greenCheckboxChecked,
            }}
            checked={analysis?.savedResultIndex === resultIndex}
            indeterminate={
              analysis?.savedResultIndex !== undefined &&
              analysis?.savedResultIndex !== resultIndex
            }
            onClick={(e) => {
              e.stopPropagation()
              e.preventDefault()
              analysis?.savedResultIndex !== resultIndex
                ? saveWordInterpretationIndex(resultIndex)
                : clearWordInterpretation()
            }}
          />
        )}
      </AccordionSummary>
      <Divider />
      <AccordionDetails className={classes.panelDetails}>
        {analysis ? (
          <ShowAnalysisAlternatives
            analysis={analysis}
            resultIndex={resultIndex}
            resultIndexOverride={resultIndexOverride}
            setResultIndexOverride={setResultIndexOverride}
          />
        ) : null}
      </AccordionDetails>
    </Accordion>
  )
}
