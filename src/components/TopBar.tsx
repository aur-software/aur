import React, { useState } from "react"
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles"
import clsx, { ClassValue } from "clsx"
import {
  AppBar,
  IconButton,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
  ButtonBase,
} from "@material-ui/core"
import MenuIcon from "@material-ui/icons/Menu"
import {
  buildVerseName,
  bookSource,
  decomposeVerseId,
  decomposeChapterId,
} from "../services/SourceText"
import { appMaxWidth } from "../services/Geometry"
import ExportDictionaryMenu from "./ExportDictionaryMenu"
import VerseSelectMenu from "./VerseSelectMenu"
import WordSelectMenu from "./WordSelectMenu"
import {
  buildWordDisplayStyle,
  buildWordDisplayText,
} from "../services/Scripts"
import { SettingsState } from "../services/Settings"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      flexDirection: "row",
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    toolbar: {
      maxWidth: appMaxWidth,
      flexBasis: appMaxWidth,
      marginLeft: "auto",
      marginRight: "auto",
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    titleSource: {
      width: "61.8%",
      textAlign: "center",
      marginRight: theme.spacing(6),
    },
  }),
)

export interface TopBarProps {
  extraClasses: ClassValue[]
  openSettings: () => any
  openOccurrences: (word: string) => any
  verseId: string
}

export default function TopBar(props: TopBarProps) {
  const classes = useStyles()
  const { extraClasses, openSettings, openOccurrences, verseId } = props
  const { settings } = SettingsState.useContainer()

  const chapterId = decomposeVerseId(verseId)[0]
  const bookId = decomposeChapterId(chapterId)[0]

  const [menuAnchor, setMenuAnchor] = useState<HTMLElement | null>(null)
  const [
    exportDictionaryMenuAnchor,
    setExportDictionaryMenuAnchor,
  ] = useState<HTMLElement | null>(null)
  const [
    verseSelectAnchor,
    setVerseSelectAnchor,
  ] = useState<HTMLElement | null>(null)
  const [wordSelectAnchor, setWordSelectAnchor] = useState<HTMLElement | null>(
    null,
  )

  return (
    <React.Fragment>
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, ...extraClasses)}
      >
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="open drawer menu"
            aria-haspopup="true"
            onClick={(e: React.MouseEvent<HTMLButtonElement>) =>
              setMenuAnchor(e.currentTarget)
            }
          >
            <MenuIcon />
          </IconButton>
          <ButtonBase
            style={{ height: "100%" }}
            onClick={(e: React.MouseEvent<HTMLButtonElement>) =>
              setVerseSelectAnchor(e.currentTarget)
            }
          >
            <Typography variant="h6" noWrap>
              {buildVerseName(verseId)}
            </Typography>
          </ButtonBase>
          <Typography
            className={classes.titleSource}
            style={buildWordDisplayStyle(26, settings)}
          >
            {buildWordDisplayText(bookSource(bookId), settings)}
          </Typography>
        </Toolbar>
      </AppBar>
      <Toolbar />
      <Menu
        anchorEl={menuAnchor}
        keepMounted
        open={Boolean(menuAnchor)}
        onClose={() => setMenuAnchor(null)}
      >
        <MenuItem
          onClick={() => {
            openSettings()
            setMenuAnchor(null)
          }}
        >
          Settings
        </MenuItem>
        <MenuItem
          onClick={() => {
            setWordSelectAnchor(menuAnchor)
            setMenuAnchor(null)
          }}
        >
          Search for Occurrences by Root...
        </MenuItem>
        <MenuItem
          onClick={() => {
            setExportDictionaryMenuAnchor(menuAnchor)
            setMenuAnchor(null)
          }}
        >
          Export Dictionary...
        </MenuItem>
      </Menu>
      <ExportDictionaryMenu
        anchorEl={exportDictionaryMenuAnchor}
        onClose={() => setExportDictionaryMenuAnchor(null)}
      />
      <VerseSelectMenu
        anchorEl={verseSelectAnchor}
        onClose={() => setVerseSelectAnchor(null)}
      />
      <WordSelectMenu
        anchorEl={wordSelectAnchor}
        onFinish={(word) => openOccurrences(word)}
        onClose={() => setWordSelectAnchor(null)}
      />
    </React.Fragment>
  )
}
