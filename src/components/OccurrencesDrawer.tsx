import React, { useState } from "react"
import { uniq } from "lodash"
import {
  createStyles,
  Theme,
  useTheme,
  makeStyles,
} from "@material-ui/core/styles"
import {
  Box,
  Drawer,
  Typography,
  LinearProgress,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Badge,
  Hidden,
  useMediaQuery,
} from "@material-ui/core"
import ExpandMoreIcon from "@material-ui/icons/ExpandMore"
import { SettingsState } from "../services/Settings"
import {
  buildWordDisplayStyle,
  buildWordDisplayText,
  isScriptLTR,
} from "../services/Scripts"
import {
  untransliterateToHebrewWord,
  splitWordId,
  bookName,
  buildVerseId,
  buildChapterId,
} from "../services/SourceText"
import {
  useSearchOccurrencesByRoot,
  useSearchOccurrencesByRootDetails,
  useSearchOccurrencesNearRoot,
} from "../workers/SearchWorker"
import ChipSet from "./ChipSet"
import ChipTiny from "./ChipTiny"
import { LocationState } from "../services/Location"
import { occurrencesDrawerWidth } from "../services/Geometry"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    drawer: {},
    drawerPaper: {
      width: occurrencesDrawerWidth,
      paddingTop: theme.spacing(2),
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
      backgroundColor: theme.palette.background.default,
    },
    titleRow: {
      display: "flex",
      alignItems: "baseline",
      paddingBottom: theme.spacing(0.5),
    },
    title: {
      cursor: "pointer",
      paddingRight: theme.spacing(2),
    },
    wordSummary: {
      paddingLeft: 0,
      "& .MuiButtonBase-root": { paddingLeft: 0 },
    },
    wordSummaryRow: {
      paddingLeft: 0,
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      width: "100%",
    },
    wordDetails: {
      paddingLeft: 0,
      paddingRight: 0,
    },
    wordDetailsTable: {
      width: "100%",
    },
    versePreviewRow: {
      display: "flex",
      width: "100%",
      cursor: "pointer",
    },
    versePreviewNum: {
      width: "4em",
      display: "flex",
      justifyContent: "flex-end",
      paddingRight: theme.spacing(0.5),
    },
    versePreviewText: {
      width: "100%",
      maxWidth: "100%",
      display: "flex",
      alignItems: "center",
      overflow: "hidden",
      height: "1.5em",
    },
    versePreviewTextCenter: {
      paddingLeft: theme.spacing(0.25),
      paddingRight: theme.spacing(0.25),
      color:
        theme.palette.secondary[
          theme.palette.type === "dark" ? "light" : "dark"
        ],
    },
    versePreviewTextLeft: {
      flex: 1,
      maxWidth: "fit-content",
      overflow: "hidden",
      "& p": { float: "right", whiteSpace: "nowrap" },
    },
    versePreviewTextRight: {
      flex: 1,
      maxWidth: "fit-content",
      overflow: "hidden",
      "& p": { float: "left", whiteSpace: "nowrap" },
    },
  }),
)

export interface OccurrencesDrawerProps {
  show: boolean
  onClose: () => any
  root: string | undefined
  near: string | undefined
}

export default function OccurrencesDrawer(props: OccurrencesDrawerProps) {
  const classes = useStyles()
  const theme = useTheme()
  const { show, onClose, root, near } = props
  const { settings } = SettingsState.useContainer()

  const baseResult = useSearchOccurrencesByRoot(root)?.found
  const nearResult = useSearchOccurrencesNearRoot(root, near, 0)?.found
  const searchResult = nearResult || baseResult

  const totalCount = searchResult
    ?.map(([_, { at }]) => at.length)
    .reduce((sum, count) => sum + count, 0)

  const drawer = (
    <React.Fragment>
      <Box className={classes.titleRow}>
        <Typography
          noWrap
          variant="h5"
          className={classes.title}
          onClick={onClose}
        >
          Occurrences:
        </Typography>

        <Badge
          color="primary"
          badgeContent={totalCount}
          max={totalCount}
          invisible={!totalCount}
        >
          <Typography style={buildWordDisplayStyle(30, settings)}>
            {buildWordDisplayText(
              untransliterateToHebrewWord(root || ""),
              settings,
            )}
          </Typography>
        </Badge>
      </Box>
      {searchResult && root ? (
        searchResult.map(([word, info]) => (
          <OccurrencesDrawerWord
            root={root}
            word={word}
            at={info.at}
            onClose={onClose}
          />
        ))
      ) : (
        <LinearProgress />
      )}
    </React.Fragment>
  )

  return (
    <React.Fragment>
      <Hidden xsDown implementation="js">
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor={theme.direction === "rtl" ? "right" : "left"}
          open={show}
          onClose={onClose}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          {drawer}
        </Drawer>
      </Hidden>
      <Hidden smUp implementation="js">
        <Drawer
          className={classes.drawer}
          variant="temporary"
          anchor={theme.direction === "rtl" ? "right" : "left"}
          open={show}
          onClose={onClose}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          {drawer}
        </Drawer>
      </Hidden>
    </React.Fragment>
  )
}

export interface OccurrencesDrawerWordProps {
  root: string
  word: string
  at: string[]
  onClose: () => any
}

function OccurrencesDrawerWord(props: OccurrencesDrawerWordProps) {
  const classes = useStyles()
  const { root, word, at, onClose } = props
  const { settings } = SettingsState.useContainer()

  const [expanded, setExpanded] = useState(false)

  const count = at.length
  const bookIds = uniq(at.map((wordId) => splitWordId(wordId)[0]))
  const bookNames = bookIds.map((bookId) => bookName(bookId))

  const expandIcon = (
    <Badge
      color="primary"
      badgeContent={count}
      max={count}
      invisible={expanded}
    >
      <ExpandMoreIcon />
    </Badge>
  )

  return (
    <Accordion onChange={(e, expanded) => setExpanded(expanded)}>
      <AccordionSummary className={classes.wordSummary} expandIcon={expandIcon}>
        <Box className={classes.wordSummaryRow}>
          <ChipSet>
            {bookNames.map((name) => (
              <ChipTiny key={name} label={name} />
            ))}
          </ChipSet>
          <Typography style={buildWordDisplayStyle(26, settings)}>
            {buildWordDisplayText(untransliterateToHebrewWord(word), settings)}
          </Typography>
        </Box>
      </AccordionSummary>
      <AccordionDetails className={classes.wordDetails}>
        <OccurrencesDrawerEntries
          root={root}
          word={word}
          at={at}
          onClose={onClose}
        />
      </AccordionDetails>
    </Accordion>
  )
}

export interface OccurrencesDrawerEntriesProps {
  root: string
  word: string
  at: string[]
  onClose: () => any
}

function OccurrencesDrawerEntries(props: OccurrencesDrawerEntriesProps) {
  const classes = useStyles()
  const theme = useTheme()
  const { root, word, at, onClose } = props
  const { settings } = SettingsState.useContainer()
  const { setLocation } = LocationState.useContainer()
  const ltr = isScriptLTR(settings.displayScript)
  const isDrawerPersistent = useMediaQuery(theme.breakpoints.up("sm"))
  const detailsResult = useSearchOccurrencesByRootDetails(root, word)

  const goToVerse = (verseId: string) => {
    if (!isDrawerPersistent) onClose()
    setLocation(verseId)
  }

  var lastBookId: string | undefined
  const tableRows: React.ReactNode[] = []
  at.forEach((wordId, index) => {
    const [bookId, chapterNum, verseNum /*, wordNum */] = splitWordId(wordId)
    const verseId = buildVerseId(buildChapterId(bookId, chapterNum), verseNum)
    const details = detailsResult && detailsResult[index]

    if (bookId !== lastBookId) {
      lastBookId = bookId

      tableRows.push(<ChipTiny label={bookName(bookId)} />)
    }
    tableRows.push(
      <Box
        key={wordId}
        className={classes.versePreviewRow}
        onClick={() => goToVerse(verseId)}
      >
        <Box className={classes.versePreviewNum}>
          <ChipTiny label={`${chapterNum}:${verseNum}`} />
        </Box>
        {details ? (
          <Box className={classes.versePreviewText}>
            <p className={classes.versePreviewTextLeft}>
              <Typography style={buildWordDisplayStyle(13, settings)}>
                {buildWordDisplayText(
                  (ltr ? details.pre : details.post).join(" "),
                  settings,
                )}
              </Typography>
            </p>
            <Typography
              className={classes.versePreviewTextCenter}
              style={buildWordDisplayStyle(13, settings)}
            >
              {buildWordDisplayText(
                untransliterateToHebrewWord(word),
                settings,
              )}
            </Typography>
            <p className={classes.versePreviewTextRight}>
              <Typography style={buildWordDisplayStyle(13, settings)}>
                {buildWordDisplayText(
                  (ltr ? details.post : details.pre).join(" "),
                  settings,
                )}
              </Typography>
            </p>
          </Box>
        ) : (
          <LinearProgress style={{ flex: 1 }} />
        )}
      </Box>,
    )
  })

  return <Box className={classes.wordDetailsTable}>{tableRows}</Box>
}
