import React from "react"
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles"
import clsx, { ClassValue } from "clsx"
import { AppBar, Toolbar, IconButton, Box } from "@material-ui/core"
import NavigateBeforeIcon from "@material-ui/icons/NavigateBefore"
import NavigateNextIcon from "@material-ui/icons/NavigateNext"
import { appMaxWidth } from "../services/Geometry"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      top: "auto",
      bottom: 0,
      flexDirection: "row",
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    toolbar: {
      maxWidth: appMaxWidth,
      flexBasis: appMaxWidth,
      marginLeft: "auto",
      marginRight: "auto",
    },
  }),
)

export interface BottomBarProps {
  extraClasses: ClassValue[]
  onBack: () => any
  onNext: () => any
}

export default function BottomBar(props: BottomBarProps) {
  const classes = useStyles()
  const { extraClasses, onBack, onNext } = props

  return (
    <React.Fragment>
      <Toolbar />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, ...extraClasses)}
      >
        <Toolbar className={classes.toolbar}>
          <IconButton
            aria-label="Back"
            edge="start"
            color="inherit"
            onClick={onBack}
          >
            <NavigateBeforeIcon />
          </IconButton>
          <Box style={{ flexGrow: 1 }}></Box>
          <IconButton
            aria-label="Next"
            edge="end"
            color="inherit"
            onClick={onNext}
          >
            <NavigateNextIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
    </React.Fragment>
  )
}
