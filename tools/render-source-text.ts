///
// This script is capable of outputting an entire book of source text in one
// of the supported display script transformations. The output is raw text
// without the relevant fonts applied, but with the appropriate key codes used.
//
// The following environment variables are required:
// - DISPLAY_SCRIPT - one of the scripts named in the `allScriptNames` constant.
// - BOOK_ID - a `bookId` from the list emitted by the `listBooks()` function.
//
// For example, to output the book of Isaiah in Hebrew (without niqqud):
//   env DISPLAY_SCRIPT=hebrew BOOK_ID=isa npx ts-node tools/dump-source-text.ts
//
// For the book of Proverbs in transliterated (matres lectionis) text:
//   env DISPLAY_SCRIPT=tml BOOK_ID=prov npx ts-node tools/dump-source-text.ts

import * as fs from "fs"
import {
  listEditions,
  listBooks,
  fetchBookFromFs,
  decomposeVerseId,
  decomposeChapterId,
} from "../src/services/SourceText"
import {
  ScriptName,
  buildWordDisplayText,
  buildWordDisplayStyleString,
  allScriptNames,
} from "../src/services/Scripts"
import { initialSettings, Settings } from "../src/services/Settings"

const displayScript = process.env.DISPLAY_SCRIPT
const bookId = process.env.BOOK_ID

Promise.all(
  listEditions().map(async (edition) => {
    const editionId = edition.id

    const displayScripts = allScriptNames.filter(
      (s) => !displayScript || s == displayScript,
    )

    const targetBooks = listBooks().filter(
      (info) => !bookId || info.bookId === bookId,
    )

    const books = await Promise.all(
      targetBooks.map(({ bookId }) => fetchBookFromFs(fs, editionId, bookId)),
    )

    displayScripts.forEach((s) => {
      const displayScript = s as ScriptName
      const settings: Settings = { ...initialSettings, displayScript }

      try {
        fs.mkdirSync(`public/${editionId}/rendered/${displayScript}`)
      } catch {}

      books.forEach((book) => {
        let buffer = ""

        buffer = `
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <title>AUR: ${book.name}</title>

  <!-- Here are the custom fonts we may use: -->
  <style>
    @font-face {
      font-family: 'protosemitic';
      src: url(../../../fonts/proto10.ttf) format('truetype');
    }
    @font-face {
      font-family: 'phoenician';
      src: url(../../../fonts/phnc10.ttf) format('truetype');
    }
    @font-face {
      font-family: 'obry';
      src: url(../../../fonts/obry3.ttf) format('truetype');
    }
  </style>
</head>

<body style="${buildWordDisplayStyleString(36, settings)}">
`

        book.chapters.forEach((chapter) => {
          chapter.verses.forEach((verse) => {
            const [chapterId, verseNum] = decomposeVerseId(verse.id)
            const [_, chapterNum] = decomposeChapterId(chapterId)

            buffer += `<p>${chapterNum}:${verseNum} `
            buffer += verse.words
              .map((word) => buildWordDisplayText(word.source, settings))
              .join(" ")
            buffer += "</p>\n"
          })
        })

        buffer += `</body></html>`

        fs.promises.writeFile(
          `public/${editionId}/rendered/${displayScript}/${book.id}.html`,
          buffer,
        )
      })

      fs.promises.writeFile(
        `public/${editionId}/rendered/${displayScript}/index.html`,
        `
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <title>AUR: Index</title>
</head>
<body style="font-size: 36pt">
${books
  .map((book) => `<p><a href="${book.id}.html">${book.name}</a></p>`)
  .join("\n")}
</body>
`,
      )
    })
  }),
).catch((e: any) => console.error(e))
