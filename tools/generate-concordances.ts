///
// TODO: docs

import {
  listEditions,
  listBooks,
  fetchBookFromFs,
} from "../src/services/SourceText"
import { DigOp, digWord, DigResult } from "../src/services/Dig"
const fs = require("fs")

const bookId = process.env.BOOK_ID

function writeMapArrayAsJSON(data: [string, any][], path: string) {
  const dataJSON = `[\n${data
    .map((pair: any) => JSON.stringify(pair))
    .join(",\n")}\n]`

  // Verify that it can be parsed as JSON.
  JSON.parse(dataJSON)

  fs.promises.writeFile(path, dataJSON)
}

Promise.all(
  listEditions().map(async (edition) => {
    const editionId = edition.id
    console.log(`Generating ${editionId} concordance for book:`, bookId)

    const targetBooks = listBooks().filter(
      (info) => !bookId || info.bookId === bookId,
    )

    const books = await Promise.all(
      targetBooks.map(({ bookId }) => fetchBookFromFs(fs, editionId, bookId)),
    )

    const exactWords = new Map<string, string[]>()

    books.forEach((book) => {
      book.chapters.forEach((chapter) => {
        chapter.verses.forEach((verse) => {
          verse.words.forEach((word) => {
            if (exactWords.has(word.word)) {
              exactWords.get(word.word)!.push(word.id)
            } else {
              exactWords.set(word.word, [word.id])
            }
          })
        })
      })
    })

    const exactWordsSorted = [...exactWords].sort(
      ([wordA, listA], [wordB, listB]) => {
        if (listA.length < listB.length) {
          return 1
        } else if (listA.length > listB.length) {
          return -1
        } else if (wordA < wordB) {
          return -1
        } else if (wordA > wordB) {
          return 1
        } else {
          return 0
        }
      },
    )

    writeMapArrayAsJSON(
      exactWordsSorted,
      `public/${editionId}/concord/exact.json`,
    )

    const rootAnalysis = new Map<
      string,
      Map<string, { ops: string[][]; at: string[] }>
    >()

    books.forEach((book) => {
      book.chapters.forEach((chapter) => {
        chapter.verses.forEach((verse) => {
          verse.words.forEach((word) => {
            digWord(word.word)
              .reduce((digsByRoot, dig: DigResult) => {
                const digs = digsByRoot.get(dig.root) || []
                digs.push(dig)
                digsByRoot.set(dig.root, digs)
                return digsByRoot
              }, new Map<string, DigResult[]>())
              .forEach((digs, root) => {
                let rootEntry = rootAnalysis.get(root)
                if (!rootEntry) {
                  rootEntry = new Map()
                  rootAnalysis.set(root, rootEntry)
                }

                const wordEntry = rootEntry.get(word.word)
                if (wordEntry) {
                  wordEntry.at.push(word.id)
                } else {
                  rootEntry.set(word.word, {
                    ops: digs.map((dig) => dig.ops.map((op) => op.id)),
                    at: [word.id],
                  })
                }
              })
          })
        })
      })
    })

    const rootAnalysisSorted: [string, any][] = [...rootAnalysis]
      .sort(([rootA, entryA], [rootB, entryB]) => {
        const occurA = [...entryA]
          .map(([word, e]) => e.at.length)
          .reduce((i, j) => i + j, 0)
        const occurB = [...entryB]
          .map(([word, e]) => e.at.length)
          .reduce((i, j) => i + j, 0)

        if (occurA < occurB) {
          return 1
        } else if (occurA > occurB) {
          return -1
        } else if (rootA < rootB) {
          return -1
        } else if (rootA > rootB) {
          return 1
        } else {
          return 0
        }
      })
      .map(([root, entry]) => [
        root,
        [...entry].sort(([wordA, entryA], [wordB, entryB]) => {
          const minOpsA = Math.min(...entryA.ops.map((ops) => ops.length))
          const minOpsB = Math.min(...entryB.ops.map((ops) => ops.length))

          if (entryA.at.length < entryB.at.length) {
            return 1
          } else if (entryA.at.length > entryB.at.length) {
            return -1
          } else if (minOpsA < minOpsB) {
            return -1
          } else if (minOpsA > minOpsB) {
            return 1
          } else if (wordA.length < wordB.length) {
            return -1
          } else if (wordA.length > wordB.length) {
            return 1
          } else if (wordA < wordB) {
            return -1
          } else if (wordA > wordB) {
            return 1
          } else {
            return 0
          }
        }),
      ])

    writeMapArrayAsJSON(
      rootAnalysisSorted,
      `public/${editionId}/concord/roots.json`,
    )
  }),
).catch((e: any) => console.error(e))
