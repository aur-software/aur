set -e

# The file in the current directory where the analysis database is saved/loaded.
file="init-db-analysis"

# A shorthand to apply common options to curl.
curl="curl --fail --silent --show-error"

# Create the database.
echo -n "Creating the analysis database (if it doesn't exist)... "
res=$(
  curl --silent --show-error \
    -X PUT admin:${COUCHDB_ADMIN_PASS}@localhost:5984/analysis
)
echo $res | grep -oP '(ok(?=":true)|(?<=file_)exists)' \
  || $(echo $res; exit 1)

# Load all analysis docs into couchdb that are in the saved json files.
# This is helpful when loading up a new couchdb instance from scratch.
for suffix in summary interpret translate; do
  echo -n "Loading docs from ${file}-${suffix}.json into the analysis database... "
  ${curl} admin:${COUCHDB_ADMIN_PASS}@localhost:5984/analysis/_bulk_docs \
    -H "Content-Type: application/json" -d @- \
    < "${file}-${suffix}.json"
done

# Dump all docs in the analysis database into saved json files.
# This saves any docs that were in the database, but not in the file yet.
for suffix in summary interpret translate; do
  echo -n "Saving docs to ${file}-${suffix}.json from the analysis database... "
  ${curl} 'localhost:5984/analysis/_all_docs?include_docs=true&startkey="'$suffix'"&endkey="'$suffix'_"' \
    | jq '{ new_edits: false, docs: (.rows | map(.doc))}' \
    > "${file}-${suffix}.json"
done

echo "ok"
