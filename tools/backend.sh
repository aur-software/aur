#!/usr/bin/env sh

# Start by changing the working directory to the one containing this script.
cd "$( dirname "${0}" )"

# Fail immediately when an uncaught failure occurs.
set -e

# Show all commands executed when the DEBUG environment variable is set.
test -z "${DEBUG}" || set -x

# Run couchdb in the background, managed by docker-compose.
# Use a random password for the admin user.
env \
  COUCHDB_ADMIN_PASS=$(openssl rand -hex 32) \
  docker-compose -p aur up -d --no-recreate

# Fetch the random password that was used for the container.
# Note that we do it this way instead of saving the generated one above
# because the --no-recreate flag on the docker-compose invocation specifies that
# we will not necessarily run a container with the password generated above.
COUCHDB_ADMIN_PASS=$(docker inspect aur_couchdb_1 | grep -oP '(?<=COUCHDB_ADMIN_PASS=)[^"]+')

# Set up CORS to allow all origins.
echo -n "Configuring CORS to allow all origins... "
npx add-cors-to-couchdb http://localhost:5984 -u admin -p $COUCHDB_ADMIN_PASS

# Initialize the needed databases and design documents within CouchDB.
env \
  COUCHDB_ADMIN_PASS=${COUCHDB_ADMIN_PASS} \
  sh ./init-db-analysis.sh
