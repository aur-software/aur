///
// This script downloads all books of source text that are not yet downloaded.
// They will be written as JSON files to the `public/wlc/books` folder.
//
// To run the script:
//   npx ts-node tools/download-source-text.ts

require("jsdom-global")()
const fs = require("fs")

const silent = (process.env.SILENT || "false") !== "false"

// eslint-disable-next-line import/first
import {
  listEditions,
  listBooks,
  pathForBookOnFs,
  downloadAndParseBook,
} from "../src/services/SourceText"

Promise.all(
  listBooks().map(async ({ bookId }) => {
    const editions = listEditions()
    for (const index in editions) {
      const editionId = editions[index]?.id
      const path = pathForBookOnFs(editionId, bookId)

      if (fs.existsSync(path)) {
        if (!silent) console.log(path, "... already exists")
      } else {
        await fs.promises.writeFile(
          path,
          JSON.stringify(
            await downloadAndParseBook(editionId, bookId),
            undefined,
            2,
          ),
        )
        if (!silent) console.log(path, "... downloaded")
      }
    }
  }),
).catch((e: any) => console.error(e))
